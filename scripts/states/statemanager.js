$r.setState = function( state ) {
	// handle clean up between states
	$r.buttons.length = 0;
	$r.mouse.down = 0;
	$r.states[state].setup();

	// set state
	$r.state = state;
};