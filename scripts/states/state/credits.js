$r.states['credits'] = function() {
	$r.clearScreen();

	$r.ctxmg.beginPath();
	var creditsTitle = $r.text( {
		ctx: $r.ctxmg,
		x: $r.cw / 2,
		y: 100,
		text: 'CREDITS',
		hspacing: 3,
		vspacing: 1,
		halign: 'center',
		valign: 'bottom',
		scale: 10,
		snap: 1,
		render: 1
	} );
	var gradient = $r.ctxmg.createLinearGradient( creditsTitle.sx, creditsTitle.sy, creditsTitle.sx, creditsTitle.ey );
	gradient.addColorStop( 0, '#fff' );
	gradient.addColorStop( 1, '#999' );
	$r.ctxmg.fillStyle = gradient;
	$r.ctxmg.fill();

	$r.ctxmg.beginPath();
	var creditKeys = $r.text( {
		ctx: $r.ctxmg,
		x: $r.cw / 2 - 10,
		y: creditsTitle.ey + 49,
		text: 'CREATED FOR JS13KGAMES BY\nINSPIRATION AND SUPPORT\n\nAUDIO PROCESSING\nGAME INSPIRATION AND IDEAS\n\nHTML5 CANVAS REFERENCE\n\nGAME MATH REFERENCE',
		hspacing: 1,
		vspacing: 17,
		halign: 'right',
		valign: 'top',
		scale: 2,
		snap: 1,
		render: 1
	} );
	$r.ctxmg.fillStyle = 'hsla(0, 0%, 100%, 0.5)';
	$r.ctxmg.fill();

	$r.ctxmg.beginPath();
	var creditValues = $r.text( {
		ctx: $r.ctxmg,
		x: $r.cw / 2 + 10,
		y: creditsTitle.ey + 49,
		text: '@JACKRUGILE\n@REZONER, @LOKTAR00, @END3R,\n@AUSTINHALLOCK, @CHANDLERPRALL\nJSFXR BY @MARKUSNEUBRAND\nASTEROIDS, CELL WARFARE,\nSPACE PIPS, AND MANY MORE\nNIHILOGIC HTML5\nCANVAS CHEAT SHEET\nBILLY LAMBERTA FOUNDATION\nHTML5 ANIMATION WITH JAVASCRIPT',
		hspacing: 1,
		vspacing: 17,
		halign: 'left',
		valign: 'top',
		scale: 2,
		snap: 1,
		render: 1
	} );
	$r.ctxmg.fillStyle = '#fff';
	$r.ctxmg.fill();

	$r.renderButtons();
}

$r.states['credits'].setup = function() {
	$r.mouse.down = 0;

	var js13kButton = new $r.Button( {
		x: $r.cw / 2 + 1,
		y: 476,
		lockedWidth: 299,
		lockedHeight: 49,
		scale: 3,
		title: 'JS13KGAMES',
		action: function() {
			location.href = 'http://js13kgames.com';
			$r.mouse.down = 0;
		}
	} );
	$r.buttons.push( js13kButton );

	var menuButton = new $r.Button( {
		x: $r.cw / 2 + 1,
		y: js13kButton.ey + 25,
		lockedWidth: 299,
		lockedHeight: 49,
		scale: 3,
		title: 'MENU',
		action: function() {
			$r.setState( 'menu' );
		}
	} );
	$r.buttons.push( menuButton );
}