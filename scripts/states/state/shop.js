$r.states['shop'] = function() {
	$r.clearScreen();
	$r.updateScreen();

	$r.renderButtons(null, function(button) {
		if(button.upgrade) {
			var upgr = new String($r.getUpgradeAmt(button.upgrade.id));
			var cost = $r.getUpgradeCost(button.upgrade.id);
			var maxed = $r.isUpgradeMaxxed(button.upgrade.id);

			$r.ctxmg.beginPath();

			if(!maxed) {
				$r.text( {
					ctx: $r.ctxmg,
					x: button.ex + 20,
					y: button.ey - 15,
					text: cost + ' RP',
					hspacing: 1,
					vspacing: 1,
					halign: 'left',
					valign: 'center',
					scale: 3,
					snap: 1,
					render: 1
				} );

				if(cost > $r.storage['rp']) {
					$r.ctxmg.fillStyle = '#a00';
					$r.ctxmg.fill();
					$r.ctxmg.beginPath();
				}
			}

			$r.text( {
				ctx: $r.ctxmg,
				x: $r.cw / 4,
				y: button.ey - 15,
				text: upgr,
				hspacing: 1,
				vspacing: 1,
				halign: 'right',
				valign: 'center',
				scale: 3,
				snap: 1,
				render: 1
			} );

			$r.ctxmg.fillStyle = '#0d0';
			$r.ctxmg.fill();
		}
	});

	$r.ctxmg.beginPath();
	$r.text( {
		ctx: $r.ctxmg,
		x: $r.cw / 2,
		y: $r.ch - 20,
		text: ($r.storage['rp'] || 0) + ' RP',
		hspacing: 1,
		vspacing: 1,
		halign: 'center',
		valign: 'bottom',
		scale: 2,
		snap: 1,
		render: 1
	} );
	$r.ctxmg.fillStyle = '#0d0';
	$r.ctxmg.fill();
}

$r.states['shop'].setup = function() {
	var menuButton = new $r.Button( {
		x: $r.cw / 2 + 1,
		y: $r.ch - 70,
		lockedWidth: 299,
		lockedHeight: 49,
		scale: 3,
		title: 'BACK',
		action: function() {
			$r.setState( 'menu' );
		},
	});
	$r.buttons.push( menuButton );

	var btns = [];

	var i = 0;

	for(var j = 0; j < $r.definitions.upgrades.length; j++) {
		var upgrade = $r.definitions.upgrades[j];

		btns.push( new $r.Button({
			upgrade: {
				id: upgrade.id,
				cost: upgrade.cost.initial
			},
			x: $r.cw / 2,
			y: 0,
			lockedWidth: 309,
			lockedHeight: 32,
			scale: 3,
			title: upgrade.title,
			color: upgrade.color,
			action: function() {
				$r.mouse.down = 0;
				$r.purchaseUpgrade(this.upgrade.id);
			}
		}) );
	}

	btns.sort(function(a, b) {
		return a.upgrade.cost - b.upgrade.cost;
	});

	i = 0;
	btns = btns.map(function(elem) {
		elem.setPosition(elem.x, 20 + i);
		i += 35;
		return elem;
	});

	$r.buttons = $r.buttons.concat(btns);
}