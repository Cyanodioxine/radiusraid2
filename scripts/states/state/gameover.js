$r.states['gameover'] = function() {
	$r.clearScreen();
	$r.ctxmg.putImageData( $r.screenshot, 0, 0 );

	$r.renderButtons();

	$r.ctxmg.beginPath();
	var gameoverTitle = $r.text( {
		ctx: $r.ctxmg,
		x: $r.cw / 2,
		y: 150,
		text: 'GAME OVER',
		hspacing: 3,
		vspacing: 1,
		halign: 'center',
		valign: 'bottom',
		scale: 10,
		snap: 1,
		render: 1
	} );
	var gradient = $r.ctxmg.createLinearGradient( gameoverTitle.sx, gameoverTitle.sy, gameoverTitle.sx, gameoverTitle.ey );
	gradient.addColorStop( 0, '#f22' );
	gradient.addColorStop( 1, '#b00' );
	$r.ctxmg.fillStyle = gradient;
	$r.ctxmg.fill();

	$r.ctxmg.beginPath();
	var gameoverStatsKeys = $r.text( {
		ctx: $r.ctxmg,
		x: $r.cw / 2 - 10,
		y: gameoverTitle.ey + 51,
		text: 'SCORE\nLEVEL\nKILLS\nBULLETS\nPOWERUPS\nTIME',
		hspacing: 1,
		vspacing: 17,
		halign: 'right',
		valign: 'top',
		scale: 2,
		snap: 1,
		render: 1
	} );
	$r.ctxmg.fillStyle = 'hsla(0, 0%, 100%, 0.5)';
	$r.ctxmg.fill();

	$r.ctxmg.beginPath();
	var gameoverStatsValues = $r.text( {
		ctx: $r.ctxmg,
		x: $r.cw / 2 + 10,
		y: gameoverTitle.ey + 51,
		text:
		$r.util.commas( $r.score ) + '\n' +
		( $r.level.current + 1 ) + '\n' +
		$r.util.commas( $r.kills ) + '\n' +
		$r.util.commas( $r.bulletsFired ) + '\n' +
		$r.util.commas( $r.powerupsCollected ) + '\n' +
		$r.util.convertTime( ( $r.elapsed * ( 1000 / 60 ) ) / 1000 )
		,
		hspacing: 1,
		vspacing: 17,
		halign: 'left',
		valign: 'top',
		scale: 2,
		snap: 1,
		render: 1
	} );
	$r.ctxmg.fillStyle = '#fff';
	$r.ctxmg.fill();
}

$r.states['gameover'].setup = function() {
	$r.mouse.down = 0;

	$r.screenshot = $r.ctxmg.getImageData( 0, 0, $r.cw, $r.ch );
	var resumeButton = new $r.Button( {
		x: $r.cw / 2 + 1,
		y: 426,
		lockedWidth: 299,
		lockedHeight: 49,
		scale: 3,
		title: 'PLAY AGAIN',
		action: function() {
			$r.reset();
			$r.audio.play( 'levelup' );
			$r.setState( 'play' );
		}
	} );
	$r.buttons.push( resumeButton );

	var menuButton = new $r.Button( {
		x: $r.cw / 2 + 1,
		y: resumeButton.ey + 25,
		lockedWidth: 299,
		lockedHeight: 49,
		scale: 3,
		title: 'MENU',
		action: function() {
			$r.setState( 'menu' );
		}
	} );
	$r.buttons.push( menuButton );

	$r.storage['score'] = Math.max( $r.storage['score'], $r.score );
	$r.storage['level'] = Math.max( $r.storage['level'], $r.level.current );
	$r.storage['rounds'] += 1;
	$r.storage['kills'] += $r.kills;
	$r.storage['bullets'] += $r.bulletsFired;
	$r.storage['powerups'] += $r.powerupsCollected;
	$r.storage['time'] += Math.floor( $r.elapsed );
	$r.updateStorage();
}