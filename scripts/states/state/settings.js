$r.states['settings'] = function() {
	$r.clearScreen();
	$r.updateScreen();
	$r.renderButtons(null, function(button) {
		if(button.control) {
			$r.ctxmg.beginPath();
			$r.text( {
				ctx: $r.ctxmg,
				x: button.ex + 15,
				y: button.ey - 20,
				text: button.control,
				hspacing: 1,
				vspacing: 1,
				halign: 'left',
				valign: 'bottom',
				scale: 2,
				snap: 1,
				render: 1
			} );
			$r.ctxmg.fillStyle = '#fff';
			$r.ctxmg.fill();
		}
	});
};

$r.states['settings'].setup = function() {
	$r.mouse.down = 0;

	var menuButton = new $r.Button( {
		x: $r.cw / 4,
		y: $r.ch - 70,
		lockedWidth: 299,
		lockedHeight: 49,
		scale: 3,
		title: 'BACK',
		action: function() {
			$r.setState( 'menu' );
		},
	});
	$r.buttons.push( menuButton );

	var clearButton = new $r.Button( {
		x: $r.cw / 4,
		y: 60,
		lockedWidth: 299,
		lockedHeight: 49,
		scale: 3,
		title: 'CLEAR DATA',
		action: function() {
			$r.mouse.down = 0;
			if( window.confirm( 'Are you sure you want to clear all locally stored game data? This cannot be undone.') ) {
				$r.clearStorage();
				$r.mouse.down = 0;
			}
		}
	} );
	$r.buttons.push( clearButton );

	var toggleMinimap = new $r.Button( {
		x: $r.cw / 4,
		y: clearButton.ey + 32,
		lockedWidth: 299,
		lockedHeight: 49,
		scale: 3,
		title: 'MINIMAP: ' + ($r.minimap.visible ? 'ON' : 'OFF'),
		action: function() {
			$r.mouse.down = 0;
			$r.minimap.visible = !$r.minimap.visible;
			$r.setStorageValue('showMinimap', $r.minimap.visible);

			this.title = 'MINIMAP: ' + ($r.minimap.visible ? 'ON' : 'OFF');
		}
	} );
	$r.buttons.push( toggleMinimap );

	var hardMode = new $r.Button( {
		x: $r.cw / 4,
		y: toggleMinimap.ey + 32,
		lockedWidth: 299,
		lockedHeight: 49,
		scale: 3,
		title: 'HARD MODE: ' + ($r.hardMode ? 'ON' : 'OFF'),
		action: function() {
			$r.mouse.down = 0;
			$r.toggleHardMode();
			$r.setStorageValue('hard', $r.hardMode);

			this.title = 'HARD MODE: ' + ($r.hardMode ? 'ON' : 'OFF');
		}
	} );
	$r.buttons.push( hardMode );


	var dy = 0;
	for(var key in $r.definitions.controls) {
		var def = $r.definitions.controls[key];
		var cur = $r.storage.controls[key];

		$r.buttons.push( new $r.Button( {
			control: def.name.toUpperCase(),
			x: ($r.cw * 2) / 3 - 30,
			y: 60 + dy,
			lockedWidth: 120,
			lockedHeight: 49,
			scale: 3,
			title: def.default.toUpperCase(),
			action: function() {
				$r.mouse.down = 0;
				alert('Control changing coming soon.')
			}
		} ) );

		dy += 53;
	}
}