$r.states['stats'] = function() {
	$r.clearScreen();

	$r.ctxmg.beginPath();
	var statsTitle = $r.text( {
		ctx: $r.ctxmg,
		x: $r.cw / 2,
		y: 150,
		text: 'STATS',
		hspacing: 3,
		vspacing: 1,
		halign: 'center',
		valign: 'bottom',
		scale: 10,
		snap: 1,
		render: 1
	} );
	var gradient = $r.ctxmg.createLinearGradient( statsTitle.sx, statsTitle.sy, statsTitle.sx, statsTitle.ey );
	gradient.addColorStop( 0, '#fff' );
	gradient.addColorStop( 1, '#999' );
	$r.ctxmg.fillStyle = gradient;
	$r.ctxmg.fill();

	$r.ctxmg.beginPath();
	var statKeys = $r.text( {
		ctx: $r.ctxmg,
		x: $r.cw / 2 - 10,
		y: statsTitle.ey + 39,
		text: 'BEST SCORE\nBEST LEVEL\nROUNDS PLAYED\nENEMIES KILLED\nBULLETS FIRED\nPOWERUPS COLLECTED\nTIME ELAPSED',
		hspacing: 1,
		vspacing: 17,
		halign: 'right',
		valign: 'top',
		scale: 2,
		snap: 1,
		render: 1
	} );
	$r.ctxmg.fillStyle = 'hsla(0, 0%, 100%, 0.5)';
	$r.ctxmg.fill();

	$r.ctxmg.beginPath();
	var statsValues = $r.text( {
		ctx: $r.ctxmg,
		x: $r.cw / 2 + 10,
		y: statsTitle.ey + 39,
		text:
		$r.util.commas( $r.storage['score'] ) + '\n' +
		( $r.storage['level'] + 1 ) + '\n' +
		$r.util.commas( $r.storage['rounds'] ) + '\n' +
		$r.util.commas( $r.storage['kills'] ) + '\n' +
		$r.util.commas( $r.storage['bullets'] ) + '\n' +
		$r.util.commas( $r.storage['powerups'] ) + '\n' +
		$r.util.convertTime( ( $r.storage['time'] * ( 1000 / 60 ) ) / 1000 )
		,
		hspacing: 1,
		vspacing: 17,
		halign: 'left',
		valign: 'top',
		scale: 2,
		snap: 1,
		render: 1
	} );
	$r.ctxmg.fillStyle = '#fff';
	$r.ctxmg.fill();

	$r.renderButtons();
}

$r.states['stats'].setup = function() {
	$r.mouse.down = 0;

	var menuButton = new $r.Button( {
		x: $r.cw / 2 + 1,
		y: 450,
		lockedWidth: 299,
		lockedHeight: 49,
		scale: 3,
		title: 'BACK',
		action: function() {
			$r.setState( 'menu' );
		}
	} );
	$r.buttons.push( menuButton );
}