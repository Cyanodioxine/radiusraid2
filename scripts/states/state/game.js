$r.states['play'] = function() {
	$r.updateDelta();
	$r.updateScreen();
	$r.updateLevel();
	$r.updatePowerupTimers();
	$r.spawnEnemies();

	if($r.emp) {
		$r.enemyOffsetMod += $r.dt / 10;
	} else {
		$r.enemyOffsetMod += ( $r.slow ) ? $r.dt / 3 : $r.dt;
	}


	// update entities
	var i = $r.enemies.length; while( i-- ){ $r.enemies[ i ].update( i ) }
	i = $r.explosions.length; while( i-- ){ $r.explosions[ i ].update( i ) }
	i = $r.powerups.length; while( i-- ){ $r.powerups[ i ].update( i ) }
	i = $r.particleEmitters.length; while( i-- ){ $r.particleEmitters[ i ].update( i ) }
	i = $r.textPops.length; while( i-- ){ $r.textPops[ i ].update( i ) }
	i = $r.bullets.length; while( i-- ){ $r.bullets[ i ].update( i ) }
	$r.hero.update();
	$r.levelUI.update();

	// render entities
	$r.clearScreen();
	$r.ctxmg.save();
	$r.ctxmg.translate( $r.screen.x - $r.rumble.x, $r.screen.y - $r.rumble.y );
	i = $r.enemies.length; while( i-- ){ $r.enemies[ i ].render( i ) }
	i = $r.explosions.length; while( i-- ){ $r.explosions[ i ].render( i ) }
	i = $r.powerups.length; while( i-- ){ $r.powerups[ i ].render( i ) }
	i = $r.particleEmitters.length; while( i-- ){ $r.particleEmitters[ i ].render( i ) }
	i = $r.textPops.length; while( i-- ){ $r.textPops[ i ].render( i ) }
	i = $r.bullets.length; while( i-- ){ $r.bullets[ i ].render( i ) }
	$r.hero.render();
	$r.ctxmg.restore();
	$r.levelUI.render();
	$r.renderInterface();
	//$r.renderMinimap();
	renderInterfaces();
	$r.renderButtons();

	$r.ctxmg.beginPath();
	$r.text( {
		ctx: $r.ctxmg,
		x: $r.cw / 2,
		y: $r.ch - 10,
		text: ($r.storage['rp'] || 0) + ' RP',
		hspacing: 1,
		vspacing: 1,
		halign: 'center',
		valign: 'bottom',
		scale: 1.7,
		snap: 1,
		render: 1
	} );
	$r.ctxmg.fillStyle = '#0d0';
	$r.ctxmg.fill();

	// handle gameover
	if( $r.hero.life <= 0 ) {
		var alpha = ( ( $r.gameoverTick / $r.gameoverTickMax ) * 0.8 );
		alpha = Math.min( 1, Math.max( 0, alpha ) );
		$r.ctxmg.fillStyle = 'hsla(0, 100%, 0%, ' + alpha + ')';
		$r.ctxmg.fillRect( 0, 0, $r.cw, $r.ch );
		if( $r.gameoverTick < $r.gameoverTickMax ){
			$r.gameoverTick += $r.dt;
		} else {
			$r.setState( 'gameover' );
		}

		if( !$r.gameoverExplosion ) {
			$r.audio.play( 'death' );
			$r.rumble.level = 25;
			$r.explosions.push( new $r.Explosion( {
				x: $r.hero.x + $r.util.rand( -10, 10 ),
				y: $r.hero.y + $r.util.rand( -10, 10 ),
				radius: 50,
				hue: 0,
				saturation: 0
			} ) );
			$r.particleEmitters.push( new $r.ParticleEmitter( {
				x: $r.hero.x,
				y: $r.hero.y,
				count: 45,
				spawnRange: 10,
				friction: 0.95,
				minSpeed: 2,
				maxSpeed: 20,
				minDirection: 0,
				maxDirection: $r.twopi,
				hue: 0,
				saturation: 0
			} ) );
			for( var i = 0; i < $r.powerupTimers.length; i++ ){
				$r.powerupTimers[ i ] = 0;
			}
			$r.gameoverExplosion = 1;
		}
	}

	// update tick
	$r.tick += $r.dt;

	// listen for pause
	if( $r.keys.pressed.p ){
		$r.setState( 'pause' );
	}

	// always listen for autofire toggle
	if( $r.keys.pressed.f ){
		$r.autofire = ~~!$r.autofire;
		$r.storage['autofire'] = $r.autofire;
		$r.updateStorage();
	}
}

$r.states['play'].setup = function() { }