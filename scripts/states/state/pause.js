$r.states['pause'] = function() {
	$r.clearScreen();
	$r.ctxmg.putImageData( $r.screenshot, 0, 0 );

	$r.ctxmg.fillStyle = 'hsla(0, 0%, 0%, 0.4)';
	$r.ctxmg.fillRect( 0, 0, $r.cw, $r.ch );

	$r.ctxmg.beginPath();
	var pauseText = $r.text( {
		ctx: $r.ctxmg,
		x: $r.cw / 2,
		y: $r.ch / 2 - 50,
		text: 'PAUSED',
		hspacing: 3,
		vspacing: 1,
		halign: 'center',
		valign: 'bottom',
		scale: 10,
		snap: 1,
		render: 1
	} );
	var gradient = $r.ctxmg.createLinearGradient( pauseText.sx, pauseText.sy, pauseText.sx, pauseText.ey );
	gradient.addColorStop( 0, '#fff' );
	gradient.addColorStop( 1, '#999' );
	$r.ctxmg.fillStyle = gradient;
	$r.ctxmg.fill();

	$r.renderButtons();

	if( $r.keys.pressed.p ){
		$r.setState( 'play' );
	}
}

$r.states['pause'].setup = function() {
	$r.mouse.down = 0;
	$r.screenshot = $r.ctxmg.getImageData( 0, 0, $r.cw, $r.ch );
	var resumeButton = new $r.Button( {
		x: $r.cw / 2 + 1,
		y: $r.ch / 2 + 26,
		lockedWidth: 299,
		lockedHeight: 49,
		scale: 3,
		title: 'RESUME',
		action: function() {
			$r.lt = Date.now() + 1000;
			$r.setState( 'play' );
		}
	} );
	$r.buttons.push( resumeButton );

	var menuButton = new $r.Button( {
		x: $r.cw / 2 + 1,
		y: resumeButton.ey + 25,
		lockedWidth: 299,
		lockedHeight: 49,
		scale: 3,
		title: 'MENU',
		action: function() {
			$r.mouse.down = 0;
			if( window.confirm( 'Are you sure you want to end this game and return to the menu?') ) {
				$r.mousescreen();
				$r.setState( 'menu' );
			}
		}
	} );
	$r.buttons.push( menuButton );
}