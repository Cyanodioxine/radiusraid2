$r.states['menu'] = function() {
	$r.clearScreen();
	$r.updateScreen();
	$r.states['menu'].renderEnemy();

	$r.ctxmg.beginPath();
	$r.ctxmg.fillStyle = '#00000099';
	$r.ctxmg.fillRect(0, 0, 250, $r.ch);
	$r.ctxmg.fill();

	$r.ctxmg.beginPath();
	var titleVersion = $r.text( {
		ctx: $r.ctxmg,
		x: 125,
		y: 100,
		text: '2',
		hspacing: 2,
		vspacing: 1,
		halign: 'center',
		valign: 'bottom',
		scale: 10,
		snap: 1,
		render: 1
	} );

	gradient = $r.ctxmg.createLinearGradient( titleVersion.sx, titleVersion.sy, titleVersion.sx, titleVersion.ey );
	gradient.addColorStop( 0, '#ff0000' );
	gradient.addColorStop( 1, '#000' );
	$r.ctxmg.fillStyle = gradient;
	$r.ctxmg.fill();

	$r.ctxmg.beginPath();
	var title = $r.text( {
		ctx: $r.ctxmg,
		x: 125,
		y: 85,
		text: 'RADIUS RAID',
		hspacing: 2,
		vspacing: 1,
		halign: 'center',
		valign: 'bottom',
		scale: 4,
		snap: 1,
		render: 1
	} );

	gradient = $r.ctxmg.createLinearGradient( title.sx, title.sy, title.sx, title.ey );
	gradient.addColorStop( 0, '#fff' );
	gradient.addColorStop( 1, '#999' );
	$r.ctxmg.fillStyle = gradient;
	$r.ctxmg.fill();

	if($r.hardMode) {
		$r.ctxmg.beginPath();
		var hm = $r.text( {
			ctx: $r.ctxmg,
			x: 125,
			y: 120,
			text: 'HARD MODE',
			hspacing: 2,
			vspacing: 1,
			halign: 'center',
			valign: 'bottom',
			scale: 2,
			snap: 1,
			render: 1
		} );

		gradient = $r.ctxmg.createLinearGradient( hm.sx, hm.sy, hm.sx, hm.ey );
		gradient.addColorStop( 0, '#ff0000' );
		gradient.addColorStop( 1, '#666' );
		$r.ctxmg.fillStyle = gradient;
		$r.ctxmg.fill();
	}

	$r.ctxmg.beginPath();

	var cashMoney = $r.text( {
		ctx: $r.ctxmg,
		x: 270,
		y: 300,
		text: $r.storage['rp'] + ' RP',
		hspacing: 1,
		vspacing: 1,
		halign: 'left',
		valign: 'center',
		scale: 3,
		snap: 1,
		render: 1
	} );

	$r.ctxmg.fillStyle = '#0d0';
	$r.ctxmg.fill();

	$r.ctxmg.beginPath();
	var bottomInfoTwo = $r.text( {
		ctx: $r.ctxmg,
		x: (2 * $r.cw) / 3,
		y: $r.ch - 10,
		text: 'REVAMPED BY THE NEOLUTIONIST',
		hspacing: 1,
		vspacing: 1,
		halign: 'center',
		valign: 'bottom',
		scale: 1.7,
		snap: 1,
		render: 1
	} );
	$r.ctxmg.fillStyle = '#a80';
	$r.ctxmg.fill();

	$r.ctxmg.beginPath();
	var bottomInfo = $r.text( {
		ctx: $r.ctxmg,
		x: (2 * $r.cw) / 3,
		y: $r.ch - 30,
		text: 'CREATED BY JACK RUGILE FOR JS13KGAMES 2013',
		hspacing: 1,
		vspacing: 1,
		halign: 'center',
		valign: 'bottom',
		scale: 1,
		snap: 1,
		render: 1
	} );
	$r.ctxmg.fillStyle = '#666';
	$r.ctxmg.fill();

	$r.renderButtons();
}

$r.states['menu'].renderEnemyOffset = 0;

$r.states['menu'].renderEnemy = function() {
	$r.ctxmg.beginPath();
	var x = ($r.cw * 2) / 3;
	var y = $r.ch / 2;
	var radius = 80;
	var hovering = $r.util.distance( $r.mouse.sx, $r.mouse.sy, x, y) <= radius;

	if($r.hardMode) {
		var hx = x + (100 * Math.cos($r.states['menu'].renderEnemyOffset / 200));
		var hy = y + (100 * Math.sin($r.states['menu'].renderEnemyOffset / 200));

		var strokeStyle = '#ffb700';
		var fillStyle = '#ffb70022';

		var mod = $r.states['menu'].renderEnemyOffset / 24;
		$r.util.fillCircle( $r.ctxmg, hx, hy, radius, fillStyle );
		$r.util.strokeCircle( $r.ctxmg,  hx, hy, radius / 4 + Math.cos( mod ) * radius / 4, strokeStyle, 1.5 );
		$r.util.strokeCircle( $r.ctxmg,  hx, hy, radius - 0.5, strokeStyle, 1 );

		$r.ctxmg.strokeStyle = strokeStyle;
		$r.ctxmg.lineWidth = 4;
		$r.ctxmg.beginPath();
		$r.ctxmg.arc( hx, hy, radius - 0.5, mod + $r.pi, mod + $r.pi + $r.pi / 2 );
		$r.ctxmg.stroke();
		$r.ctxmg.beginPath();
		$r.ctxmg.arc( hx, hy, radius - 0.5, mod, mod + $r.pi / 2 );
		$r.ctxmg.stroke();

		$r.states['menu'].renderEnemyOffset += 2;
	} else {
		var strokeStyle = !hovering ? '#00ffff' : '#ff0000';
		var fillStyle = !hovering ? '#00ffff22' : '#ff000022';

		var mod = $r.states['menu'].renderEnemyOffset / 24;
		$r.util.fillCircle( $r.ctxmg, x, y, radius, fillStyle );
		$r.util.strokeCircle( $r.ctxmg,  x, y, radius / 4 + Math.cos( mod ) * radius / 4, strokeStyle, 1.5 );
		$r.util.strokeCircle( $r.ctxmg,  x, y, radius - 0.5, strokeStyle, 1 );

		$r.ctxmg.strokeStyle = strokeStyle;
		$r.ctxmg.lineWidth = 4;
		$r.ctxmg.beginPath();
		$r.ctxmg.arc( x, y, radius - 0.5, mod + $r.pi, mod + $r.pi + $r.pi / 2 );
		$r.ctxmg.stroke();
		$r.ctxmg.beginPath();
		$r.ctxmg.arc( x, y, radius - 0.5, mod, mod + $r.pi / 2 );
		$r.ctxmg.stroke();

		$r.states['menu'].renderEnemyOffset += (hovering ? 2 : 1);
	}
}

$r.states['menu'].setup = function() {
	$r.mouse.ax = 0;
	$r.mouse.ay = 0;

	$r.reset();

	var playButton = new $r.Button( {
		x: 125,
		y: 250,
		lockedWidth: 240,
		lockedHeight: 49,
		scale: 3,
		title: 'PLAY',
		action: function() {
			$r.reset();
			$r.audio.play( 'levelup' );
			$r.setState( 'play' );
		}
	} );
	$r.buttons.push( playButton );

	var shopButton = new $r.Button( {
		x: 125,
		y: playButton.ey + 25,
		lockedWidth: 240,
		lockedHeight: 49,
		scale: 3,
		title: 'UPGRADES',
		action: function() {
			$r.setState( 'shop' );
		}
	} );
	$r.buttons.push( shopButton );

	var statsButton = new $r.Button( {
		x: 125,
		y: shopButton.ey + 27,
		lockedWidth: 240,
		lockedHeight: 49,
		scale: 3,
		title: 'STATS',
		action: function() {
			$r.setState( 'stats' );
		}
	} );
	$r.buttons.push( statsButton );

	var settingsButton = new $r.Button( {
		x: 125,
		y: statsButton.ey + 26,
		lockedWidth: 240,
		lockedHeight: 49,
		scale: 3,
		title: 'OPTIONS',
		action: function() {
			$r.setState( 'settings' );
		}
	} ) ;
	$r.buttons.push( settingsButton );

	var creditsButton = new $r.Button( {
		x: 125,
		y: $r.ch - 30,
		lockedWidth: 120,
		lockedHeight: 32,
		scale: 2,
		title: 'CREDITS',
		action: function() {
			$r.setState( 'credits' );
		}
	} ) ;
	$r.buttons.push( creditsButton );
}