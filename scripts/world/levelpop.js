/*==============================================================================
Init
==============================================================================*/
$r.LevelPop = function( opt ) {
	for( var k in opt ) {
		this[k] = opt[k];
	}
	this.level = 1;
	this.x = $r.cw - 20;
	this.y = $r.ch - 20;
	this.tick = 0;
	this.tickMax = 240;
	this.baseAlpha = 0.2;
	if( $r.tick != 0 ) {
		$r.audio.play( 'levelup' );
	}
	this.size = 12;
	this.baseSize = 12;
};

/*==============================================================================
Update
==============================================================================*/
$r.LevelPop.prototype.update = function( i ) {
	if( this.tick >= this.tickMax ) {
		$r.levelPops.splice( i, 1 );
	} else {
		this.tick += $r.dt;
	}
};

$r.LevelPop.prototype.update = function() {
	this.tick += $r.dt;
}

$r.LevelPop.prototype.render = function() {
	$r.ctxmg.beginPath();
	
	if(this.size <= 12 && this.size > 5)
	{
		this.size -= 0.05;
	}
	
	var text = $r.text( {
		ctx: $r.ctxmg,
		x: this.x,
		y: this.y,
		text: $r.util.pad( $r.level.current + 1, 2 ),
		hspacing: 3,
		vspacing: 0,
		halign: 'right',
		valign: 'bottom',
		scale: this.size,
		snap: 1,
		render: 1
	} );
	
	$r.ctxmg.fillStyle = 'hsla(0, 0%, 100%, 0.5)';
	$r.ctxmg.fill();
}