$r.renderBackground1 = function() {
	var gradient = $r.ctxbg1.createRadialGradient( $r.cbg1.width / 2, $r.cbg1.height / 2, 0, $r.cbg1.width / 2, $r.cbg1.height / 2, $r.cbg1.height );
	gradient.addColorStop( 0, 'hsla(0, 0%, 100%, 0.1)' );
	gradient.addColorStop( 0.65, 'hsla(0, 0%, 100%, 0)' );
	$r.ctxbg1.fillStyle = gradient;
	$r.ctxbg1.fillRect( 0, 0, $r.cbg1.width, $r.cbg1.height );

	var i = 2000;
	while( i-- ) {
		$r.util.fillCircle( $r.ctxbg1, $r.util.rand( 0, $r.cbg1.width ), $r.util.rand( 0, $r.cbg1.height ), $r.util.rand( 0.2, 0.5 ), 'hsla(0, 0%, 100%, ' + $r.util.rand( 0.05, 0.2 ) + ')' );
	}

	var i = 800;
	while( i-- ) {
		$r.util.fillCircle( $r.ctxbg1, $r.util.rand( 0, $r.cbg1.width ), $r.util.rand( 0, $r.cbg1.height ), $r.util.rand( 0.1, 0.8 ), 'hsla(0, 0%, 100%, ' + $r.util.rand( 0.05, 0.5 ) + ')' );
	}
}

$r.renderBackground2 = function() {
	var i = 80;
	while( i-- ) {
		$r.util.fillCircle( $r.ctxbg2, $r.util.rand( 0, $r.cbg2.width ), $r.util.rand( 0, $r.cbg2.height ), $r.util.rand( 1, 2 ), 'hsla(0, 0%, 100%, ' + $r.util.rand( 0.05, 0.15 ) + ')' );
	}
}

$r.renderBackground3 = function() {
	var i = 40;
	while( i-- ) {
		$r.util.fillCircle( $r.ctxbg3, $r.util.rand( 0, $r.cbg3.width ), $r.util.rand( 0, $r.cbg3.height ), $r.util.rand( 1, 2.5 ), 'hsla(0, 0%, 100%, ' + $r.util.rand( 0.05, 0.1 ) + ')' );
	}
}

$r.renderBackground4 = function() {
	var size = 50;
	$r.ctxbg4.fillStyle = ($r.hardMode) ? 'hsla(0, 100%, 50%, 0.5)' : 'hsla(0, 0%, 50%, 0.05)';
	var i = (0.5 + ( $r.cbg4.height / size )) | 0;
	while( i-- ) {
		$r.ctxbg4.fillRect( 0, i * size + 25, $r.cbg4.width, 1 );
	}
	i = (0.5 + ( $r.cbg4.width / size )) | 0;
	while( i-- ) {
		$r.ctxbg4.fillRect( i * size, 0, 1, $r.cbg4.height );
	}
}
