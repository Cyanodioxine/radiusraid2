/*==============================================================================
Init
==============================================================================*/
$r.TextPop = function( opt ) {
	this.alpha = 2;

	for( var k in opt ) {
		this[k] = opt[k];
	}

	this.snap = (typeof(this.snap) !== 'undefined' ? (this.snap ? 1 : 0) : 0);
	this.prefix = (opt.hasOwnProperty('prefix') ? opt.prefix : '+')

	this.vy = 0;
};

/*==============================================================================
Update
==============================================================================*/
$r.TextPop.prototype.update = function( i ) {
	this.vy -= 0.05;
	this.y += this.vy * $r.dt;
	this.alpha -= 0.03 * $r.dt;

	if( this.alpha <= 0 ){
		$r.textPops.splice( i, 1 );
	}
};

/*==============================================================================
Render
==============================================================================*/
$r.TextPop.prototype.render = function( i ) {
	if(this.overlay) {
		$r.ctxmg.resetTransform();
	}

	$r.ctxmg.beginPath();
	$r.text( {
		ctx: $r.ctxmg,
		x: this.x,
		y: this.y,
		text: this.prefix + this.value,
		hspacing: 1,
		vspacing: 1,
		halign: 'center',
		valign: 'center',
		scale: 2,
		snap: this.snap,
		render: 1
	} );
	$r.ctxmg.fillStyle = (this.color) ? this.color : 'hsla(' + this.hue + ', ' + this.saturation + '%, ' + this.lightness + '%, ' + this.alpha + ')';
	$r.ctxmg.fill();

	if(this.overlay) {
		$r.ctxmg.translate( $r.screen.x - $r.rumble.x, $r.screen.y - $r.rumble.y );
	}
}