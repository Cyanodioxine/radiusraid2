$r.definitions.levels = [];

$r.definitions.levels.repopulate = function() {
	var base = $r.hardMode ? 100 : 25;

	for( var i = 0; i < $r.definitions.enemies.length; i++ )
	{
		var distribution = [];
		for( var di = 0; di < i + 1; di++ )
		{
			var value = ( di == i ) ? Math.floor( ( ( i + 1) * base ) * 0.75 ) : ( i + 1) * base;
			value = ( i == 0 ) ? base : value;
			distribution.push( value );
		}
		$r.definitions.levels.push(
			{
				killsToLevel: 10 + ( i + 1 ) * 7,
				//killsToLevel: 10 + (i + 1),
				//killsToLevel: 10 + i,
				distribution: distribution
			} );
	}
}

$r.definitions.levels.repopulate();

$r.incrementLevel = function(amt) {
	if(!amt) amt = 1;

	var over10 = $r.level.current < 9 && $r.level.current + amt >= 9;
	var ol = $r.level.current;

	if( $r.level.current + amt < $r.levelCount ){
		$r.level.current += amt;
		$r.level.kills = 0;
		$r.level.killsToLevel = $r.definitions.levels[ $r.level.current ].killsToLevel;
		$r.level.distribution = $r.definitions.levels[ $r.level.current ].distribution;
		$r.level.distributionCount = $r.level.distribution.length;
	} else {
		$r.level.current += amt;
		$r.level.kills = 0;
		// no more level definitions, so take the last level and increase the spawn rate slightly
		//for( var i = 0; i < $r.level.distributionCount; i++ ) {
		//	$r.level.distribution[ i ] = Math.max( 1, $r.level.distribution[ i ] - 5 );
		//}
	}

	$r.levelDiffOffset = $r.level.current + 1 - $r.levelCount;
	$r.levelUI.size = 12;

	if($r.level.current >= 9) {
		if(over10) {
			console.log('Begun awarding RP for level.');
			console.log('Starting with ' + 100 * (10 - ol));
			$r.addMoney(100 * (10 - ol));
		} else $r.addMoney(100 * amt);
	}
}

$r.updateLevel = function() {
	if( $r.level.kills >= $r.level.killsToLevel ) {
		$r.incrementLevel();
	}
};