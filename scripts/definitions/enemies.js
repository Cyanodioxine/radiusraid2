$r.definitions.enemies = [
	{ // Enemy 0 - horizontal / vertical	
		value: 5,
		speed: 1.5,
		life: 1,
		radius: 15,
		hue: 180,
		lockBounds: 1,
		setup: function() {
			if( this.start == 'top' ){
				this.direction = $r.pi / 2;
			} else if( this.start == 'right' ) {
				this.direction = -$r.pi;
			} else if( this.start == 'bottom' ) {
				this.direction = -$r.pi / 2;
			} else {
				this.direction = 0;
			}
		},
		behavior: function() {
			var speed = this.speed;
			if( $r.slow ) {
				speed = this.speed / $r.slowEnemyDivider;
			}

			this.vx = Math.cos( this.direction ) * speed;
			this.vy = Math.sin( this.direction ) * speed;
		}
	},	
	{ // Enemy 1 - diagonal	
		value: 10,
		speed: 1.5,
		life: 2,
		radius: 15,
		hue: 120,
		lockBounds: 1,
		setup: function() {
			var rand = Math.floor( $r.util.rand( 0, 2 ) );
			if( this.start == 'top' ){				
				this.direction = ( rand ) ? $r.pi / 2 + $r.pi / 4: $r.pi / 2 - $r.pi / 4;
			} else if( this.start == 'right' ) {
				this.direction = ( rand ) ? -$r.pi + $r.pi / 4 : -$r.pi - $r.pi / 4;
			} else if( this.start == 'bottom' ) {
				this.direction = ( rand ) ? -$r.pi / 2 + $r.pi / 4 : -$r.pi / 2 - $r.pi / 4;
			} else {
				this.direction = ( rand ) ? $r.pi / 4 : -$r.pi / 4;
			}
		},
		behavior: function() {
			var speed = this.speed;
			if( $r.slow ) {
				speed = this.speed / $r.slowEnemyDivider;
			}

			this.vx = Math.cos( this.direction ) * speed;
			this.vy = Math.sin( this.direction ) * speed;
		}
	},
	{ // Enemy 2 - move directly hero
		value: 15,
		speed: 1.5,
		life: 2,
		radius: 20,
		hue: 330,
		behavior: function() {
			var speed = this.speed;
			if( $r.slow ) {
				speed = this.speed / $r.slowEnemyDivider;
			}

			var dx = $r.hero.x - this.x,
				dy = $r.hero.y - this.y,
				direction = Math.atan2( dy, dx );
			this.vx = Math.cos( direction ) * speed;
			this.vy = Math.sin( direction ) * speed;
		}
	},
	{ // Enemy 3 - splitter
		value: 20,
		speed: 0.5,
		life: 3,
		radius: 50,
		hue: 210,
		canSpawn: 1,
		behavior: function() {
			var speed = this.speed;
			if( $r.slow ) {
				speed = this.speed / $r.slowEnemyDivider;
			}

			var dx = $r.hero.x - this.x,
				dy = $r.hero.y - this.y,
				direction = Math.atan2( dy, dx );
			this.vx = Math.cos( direction ) * speed;
			this.vy = Math.sin( direction ) * speed;
		},
		death: function() {
			if( this.canSpawn ) {
				for( var i = 0; i < 4; i++ ) {
					var enemy = $r.spawnEnemy( this.type );
					enemy.radius = 20;
					enemy.canSpawn = 0;
					enemy.speed = 1;
					enemy.life = 1;
					enemy.value = 5;
					enemy.x = this.x;
					enemy.y = this.y;
					if( i == 0 ) {
						enemy.x -= 45;
					} else if( i == 1 ) {
						enemy.x += 45;
					} else if( i == 2 ) {
						enemy.y -= 45;
					} else {
						enemy.y += 45;
					}
					$r.enemies.push( enemy );
				}
			}
		}
	},
	{ // Enemy 4 - wanderer
		value: 25,
		speed: 2,
		life: 4,
		radius: 20,
		hue: 30,
		lockBounds: 1,
		setup: function() {
			if( this.start == 'top' ){
				this.direction = $r.pi / 2;
			} else if( this.start == 'right' ) {
				this.direction = -$r.pi;
			} else if( this.start == 'bottom' ) {
				this.direction = -$r.pi / 2;
			} else {
				this.direction = 0;
			}
		},
		behavior: function() {
			var speed = this.speed * $r.util.rand( 1, 2 );
			if( $r.slow ) {
				speed = this.speed / $r.slowEnemyDivider;
			}
			
			this.direction +=  $r.util.rand( -0.15, 0.15 );
			this.vx = Math.cos( this.direction ) * speed;
			this.vy = Math.sin( this.direction ) * speed;
		}
	},
	{ // Enemy 5 - stealth, hard to see - move directly hero
		value: 30,
		speed: 1,
		life: 3,
		radius: 20,
		hue: 0,
		saturation: 0,
		lightness: 30,
		behavior: function() {
			var speed = this.speed;
			if( $r.slow ) {
				speed = this.speed / $r.slowEnemyDivider;
			}

			var dx = $r.hero.x - this.x,
				dy = $r.hero.y - this.y,
				direction = Math.atan2( dy, dx );
			this.vx = Math.cos( direction ) * speed;
			this.vy = Math.sin( direction ) * speed;
		}
	},
	{ // Enemy 6 - big strong slow fatty
		value: 35,
		speed: 0.25,
		life: 8,
		radius: 80,
		hue: 150,
		behavior: function() {
			var speed = this.speed;
			if( $r.slow ) {
				speed = this.speed / $r.slowEnemyDivider;
			}

			var dx = $r.hero.x - this.x,
				dy = $r.hero.y - this.y,
				direction = Math.atan2( dy, dx );
			this.vx = Math.cos( direction ) * speed;
			this.vy = Math.sin( direction ) * speed;
		}
	},
	{ // Enemy 7 - small weak speedy
		value: 40,
		speed: 2.5,
		life: 1,
		radius: 15,
		hue: 300,
		behavior: function() {
			var speed = this.speed;
			if( $r.slow ) {
				speed = this.speed / $r.slowEnemyDivider;
			}

			var dx = $r.hero.x - this.x,
				dy = $r.hero.y - this.y,
				direction = Math.atan2( dy, dx );
			direction = direction + Math.cos( $r.tick / 50 ) * 1;
			this.vx = Math.cos( direction ) * speed;
			this.vy = Math.sin( direction ) * speed;
		}
	},
	{ // Enemy 8 - strong grower, move to hero
		value: 45,
		speed: 1.5,
		growth: 0.1,
		life: 6,
		radius: 20,
		hue: 0,
		saturation: 0,
		lightness: 100,
		behavior: function() {
			var speed = this.speed,
				growth = this.growth;
			if( $r.slow ) {
				speed = this.speed / $r.slowEnemyDivider;
				growth = this.growth / $r.slowEnemyDivider;
			}

			var dx = $r.hero.x - this.x,
				dy = $r.hero.y - this.y,
				direction = Math.atan2( dy, dx );			

			if( Math.sqrt(dx * dx + dy * dy ) > 200 ) {
				this.vx = Math.cos( direction ) * speed;
				this.vy = Math.sin( direction ) * speed;
				this.fillStyle ='hsla(' + this.hue + ', ' + this.saturation + '%, ' + this.lightness + '%, 0.1)';
				this.strokeStyle = 'hsla(' + this.hue + ', ' + this.saturation + '%, ' + this.lightness + '%, 1)';
			} else {
				this.vx += $r.util.rand( -0.25, 0.25 );
				this.vy += $r.util.rand( -0.25, 0.25 );
				this.radius += growth * $r.dt;
				var hue = $r.util.rand( 0, 360 );
					lightness = $r.util.rand( 50, 80 );
				this.fillStyle ='hsla(' + hue + ', 100%, ' + lightness + '%, 0.2)';
				this.strokeStyle = 'hsla(' + hue + ', 100%, ' + lightness + '%, 1)';
			}
		}
	},
	{ // Enemy 9 - circle around hero
		value: 50,
		speed: 0.5,
		angleSpeed: 0.015,
		life: 2,
		radius: 20,
		hue: 60,
		setup: function() {
			var dx = this.x - $r.hero.x,
				dy = this.y - $r.hero.y;
			this.angle = Math.atan2( dy, dx );
			this.distance = Math.sqrt( dx * dx + dy * dy );		
			if( Math.random() > 0.5 ) {
				this.angleSpeed = -this.angleSpeed;
			}
		},
		behavior: function() {
			var speed = this.speed,
				angleSpeed = this.angleSpeed;
			if( $r.slow) {
				speed = this.speed / $r.slowEnemyDivider;
				angleSpeed = this.angleSpeed / $r.slowEnemyDivider;
			}

			this.distance -= speed * $r.dt;
			this.angle += angleSpeed * $r.dt;

			this.vx = ( ( $r.hero.x + Math.cos( this.angle ) * this.distance ) - this.x ) / 50;
			this.vy = ( ( $r.hero.y + Math.sin( this.angle ) * this.distance ) - this.y ) / 50;
		}
	},
	{ // Enemy 10 - spawner
		value: 55,
		speed: 1,
		life: 3,
		radius: 45,
		hue: 0,
		canSpawn: 1,
		spawnTick: 0,
		spawnMax: 250,
		behavior: function() {
			var speed = this.speed;
			if( $r.slow ) {
				speed = this.speed / $r.slowEnemyDivider;
			}

			var dx = $r.hero.x - this.x,
				dy = $r.hero.y - this.y,
				direction = Math.atan2( dy, dx );
				direction = direction + Math.cos( $r.tick / 50 ) * 1;
			this.vx = Math.cos( direction ) * speed;
			this.vy = Math.sin( direction ) * speed;

			if( this.canSpawn ) {				
				if( this.spawnTick < this.spawnMax ) {
					this.spawnTick += $r.dt;
				} else {
					this.spawnTick = 0;
					var enemy = $r.spawnEnemy( this.type );
					enemy.radius = 20;
					enemy.canSpawn = 0;
					enemy.speed = 6;
					enemy.life = 1;
					enemy.value = 30;
					enemy.x = this.x;
					enemy.y = this.y;
					$r.enemies.push( enemy );
				}
			} 
		}
	},
	{ // Enemy 11 - random location strong tower
		value: 60,
		speed: 1.5,
		life: 10,
		radius: 30,
		hue: 90,		
		setup: function(){
			this.xTarget = $r.util.rand( 50, $r.ww - 50 );
			this.yTarget = $r.util.rand( 50, $r.wh - 50 );
		},
		behavior: function() {
			var speed = this.speed;
			if( $r.slow ) {
				speed = this.speed / $r.slowEnemyDivider;
			}
			var dx = this.xTarget - this.x,
				dy = this.yTarget - this.y,
				direction = Math.atan2( dy, dx );
			if( Math.sqrt( dx * dx + dy * dy) > this.speed ) {
				this.vx = Math.cos( direction ) * speed;
				this.vy = Math.sin( direction ) * speed;
			} else {
				this.vx = 0;
				this.vy = 0;
			}
		}
	},
	{ // Enemy 12 - speedy random direction, no homing
		value: 65,
		speed: 6,
		life: 1,
		radius: 5,
		hue: 0,
		lockBounds: 1,
		setup: function() {
			this.radius = $r.util.rand( 15, 35 );
			this.speed = $r.util.rand( 3, 8 );
			if( Math.random() > 0.5 ){
				if( this.start == 'top' ){
					this.direction = $r.pi / 2;
				} else if( this.start == 'right' ) {
					this.direction = -$r.pi;
				} else if( this.start == 'bottom' ) {
					this.direction = -$r.pi / 2;
				} else {
					this.direction = 0;
				}
			} else {
				var rand = Math.floor( $r.util.rand( 0, 2 ) );
				if( this.start == 'top' ){				
					this.direction = ( rand ) ? $r.pi / 2 + $r.pi / 4: $r.pi / 2 - $r.pi / 4;
				} else if( this.start == 'right' ) {
					this.direction = ( rand ) ? -$r.pi + $r.pi / 4 : -$r.pi - $r.pi / 4;
				} else if( this.start == 'bottom' ) {
					this.direction = ( rand ) ? -$r.pi / 2 + $r.pi / 4 : -$r.pi / 2 - $r.pi / 4;
				} else {
					this.direction = ( rand ) ? $r.pi / 4 : -$r.pi / 4;
				}
			}
		},
		behavior: function() {
			var speed = this.speed;
			if( $r.slow ) {
				speed = this.speed / $r.slowEnemyDivider;
			}
			this.vx = Math.cos( this.direction ) * speed;
			this.vy = Math.sin( this.direction ) * speed;
			this.hue += 10;
			this.lightness = 50;
			this.fillStyle = 'hsla(' + this.hue + ', 100%, ' + this.lightness + '%, 0.2)';
			this.strokeStyle = 'hsla(' + this.hue + ', 100%, ' + this.lightness + '%, 1)';
		}
	}
];

$r.getEnemiesInRadius = function(x, y, radius) {
	var enemies = [];

	var ei = $r.enemies.length;
	while( ei-- ) {
		var enemy = $r.enemies[ei];

		if($r.util.distance(x, y, enemy.x, enemy.y) <= radius + enemy.radius) {
			enemies.push({index: i, enemy: enemy})
		}
	}

	return enemies;
}

$r.spawnEnemy = function( type ) {
	var params = $r.definitions.enemies[ type ],
		coordinates = $r.getSpawnCoordinates( params.radius );
	params.x = coordinates.x;
	params.y = coordinates.y;
	params.start = coordinates.start;
	params.type = type;
	return new $r.Enemy( params );
};

$r.spawnEnemies = function() {
	var floorTick = Math.floor( $r.tick );
	for( var i = 0; i < $r.level.distributionCount; i++ ) {
		var timeCheck = $r.level.distribution[ i ];
		if( $r.levelDiffOffset > 0 ){
			timeCheck = Math.max( 1, timeCheck - ( $r.levelDiffOffset * 2) );
		}
		if( floorTick % timeCheck === 0 ) {
			$r.enemies.push( $r.spawnEnemy( i ) );
		}
	}
};