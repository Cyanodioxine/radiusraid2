$r.definitions.nuke = function() {
	$r.audio.play( 'powerup' );
	$r.audio.play( 'explosionAlt' );

	$r.explosions.push( new $r.Explosion( {
		x: $r.hero.x,
		y: $r.hero.y,
		radius: 600,
		hue: 45,
		saturation: 96
	} ) );

	var i = $r.enemies.length;

	while( i-- ) {
		var enemy = $r.enemies[i];

		if(enemy.inView) {
			enemy.receiveDamage(i, 5000);
		}
	}
}

$r.definitions.powerups = [{
		title: 'HEALTH PACK',
		hue: 0,
		saturation: 0,
		lightness: 100,
		whileActive: function() {
			if( $r.hero.life < $r.hero.maxHealth ) {
				$r.hero.life += 0.001;
			}
			if( $r.hero.life > $r.hero.maxHealth ) {
				$r.hero.life = $r.hero.maxHealth;
			}
		}
	}, {
		title: 'SLOW ENEMIES',
		hue: 200,
		saturation: 0,
		lightness: 100,
		whileActive: function() {
			$r.slow = 1;
		},
		onDisable: function() {
			$r.slow = 0;
		}
	}, {
		title: 'FAST SHOT',
		hue: 100,
		saturation: 100,
		lightness: 60,
		whileActive: function() {
			$r.hero.weapon.fireRate = 2;
			$r.hero.weapon.bullet.speed = 14;
		},
		onDisable: function() {
			$r.hero.weapon.fireRate = 5;
			$r.hero.weapon.bullet.speed = 10;
		}
	}, {
		title: 'TRIPLE SHOT',
		hue: 200,
		saturation: 100,
		lightness: 60,
		whileActive: function() {
			$r.hero.weapon.count = 3;
		},
		onDisable: function() {
			$r.hero.weapon.count = 1;
		}
	}, {
		title: 'PIERCE SHOT',
		hue: 0,
		saturation: 100,
		lightness: 60,
		whileActive: function() {
			$r.hero.weapon.bullet.piercing = 1;
		},
		onDisable: function() {
			$r.hero.weapon.bullet.piercing = 0;
		}
	}, {
		title: 'MEGA SHOT',
		hue: 45,
		saturation: 96,
		lightness: 66,
		whileActive: function() {
			$r.hero.weapon.count = 5;
			$r.hero.weapon.bullet.spread = 1.1;
		},
		onDisable: function() {
			$r.hero.weapon.count = 1;
			$r.hero.weapon.bullet.spread = 0.3;
		}
	}, {
		title: 'HYPERDRIVE',
		hue: 288,
		saturation: 100,
		lighness: 60,
		whileActive: function() {
			$r.hero.accel = 1;
		},
		onDisable: function() {
			$r.hero.accel = 0.5;
		}
	}, {
		title: 'EMP',
		hue: 0,
		saturation: 100,
		lightness: 100,
		
		whileActive: function() {
			$r.emp = 1;
		},
		onDisable: function() {
			$r.emp = 0;
		}
	}, {
		title: 'SHOTGUN',
		hue: 0,
		saturation: 100,
		lightness: 60,
		whileActive: function() {
			$r.hero.shotgun = true;
			$r.hero.weapon.count = 12;
			$r.hero.weapon.lineWidth = 5;
			$r.hero.weapon.fireRate = 50;
			$r.hero.weapon.speed = 20;
			$r.hero.weapon.size = 5;
			$r.hero.weapon.lifespan = 20;
		},
		onDisable: function() {
			$r.hero.shotgun = false;
			$r.hero.weapon.count = 1;
			$r.hero.weapon.fireRate = 5;
			$r.hero.weapon.lineWidth = 2;
			$r.hero.weapon.speed = 10;
			$r.hero.weapon.size = 15;
			$r.hero.weapon.lifespan = 500;
		}
	}
];

$r.definitions.boosters = [{
		title: 'RADIUS POINTS',
		color: '#0f0',
		booster: 1,
		use: function() {
			$r.audio.play( 'powerup' );
			$r.addMoney(100 + (150 * $r.hardMode));
		}
	}, {
		title: 'NUCLEAR DETONATOR',
		hue: 45,
		saturation: 96,
		lightness: 66,
		booster: 1,
		use: $r.definitions.nuke
	}
];

$r.spawnPowerup = function( x, y, force ) {
	if( Math.random() < 0.2 || force)
	{
		if( Math.random() < 0.1 || force ) {
			var min = ( $r.hero.life < 0.9 ) ? 0 : 1,
				type = Math.floor( $r.util.rand( min, $r.definitions.boosters.length ) ),
				params = $r.definitions.boosters[ type ];
			params.type = type;
			params.x = x;
			params.y = y;
			$r.powerups.push( new $r.Powerup( params ) );
		}
	}
	else
	{
		if( Math.random() < 0.1 ) {
			var min = ( $r.hero.life < 0.9 ) ? 0 : 1,
				type = Math.floor( $r.util.rand( min, $r.definitions.powerups.length ) ),
				params = $r.definitions.powerups[ type ];
			params.type = type;
			params.x = x;
			params.y = y;
			$r.powerups.push( new $r.Powerup( params ) );
		}
	}
};