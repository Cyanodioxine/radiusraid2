$r.definitions.controls = {
	up: {
		default: 'w',
		name: 'Move Up'
	},
	left: {
		default: 'a',
		name: 'Move Left'
	},
	down: {
		default: 's',
		name: 'Move Down'
	},
	right: {
		default: 'd',
		name: 'Move Right'
	},
	fire: {
		default: 'mouse0',
		name: 'Fire'
	},
	blink: {
		default: 'space',
		name: 'Use Blink Module'
	},
	autofire: {
		default: 'f',
		name: 'Toggle Fire'
	},
	pause: {
		default: 'esc',
		name: 'Pause Game'
	},
	cheat: {
		default: 'c',
		name: 'Cheat'
	},
	mute: {
		default: 'm',
		name: 'Mute Audio'
	},
}