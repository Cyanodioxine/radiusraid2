$r.definitions.upgrades = [{
		id: 'armor',
		title: 'ARMOR PLATING',
		start: 0,
		max: 10,
		incr: 1,

		cost: {
			initial: 5000,
			ramp: 1.5 //Multiplier
		},

		color: '#f00',
		init: function(level) {
			$r.hero.life += (0.5 * level);
			$r.hero.maxHealth += (0.5 * level);
		},
		onBuy: function(level) {
			$r.hero.maxHealth += 0.5;
		}
	}, {
		id: 'shield',
		title: 'SHIELDING',
		start: 0,
		max: 5,
		incr: 1,

		cost: {
			initial: 6000,
			ramp: 1.5 //Multiplier
		},

		color: '#bcff00',
		init: function(level) {
			$r.hero.damageDampening += (0.2 * level);
		},
		onBuy: function(level) {
			$r.hero.damageDampening += 0.2;
		}
	}, {
		id: 'ppk',
		title: 'SCAVENGER',
		start: 1,
		max: 20,
		incr: 1,

		cost: {
			initial: 8000,
			ramp: 1.17 //Multiplier
		},

		color: '#c500ff',
		init: function(level) {
			$r.definitions.ppk = level;
		},
		onBuy: function(level) {
			$r.definitions.ppk += 1;
		}
	}, {
		id: 'dmg',
		title: 'OVERCHARGE',
		start: 1,
		max: 10,
		incr: 1,

		cost: {
			initial: 10000,
			ramp: 1.5 //Multiplier
		},

		color: '#ff0',
		init: function(level) {
			$r.hero.dps = 1 + level;
		},
		onBuy: function(level) {
			$r.hero.dps += 1;
		}
	}, {
		id: 'overclocking',
		title: 'OVERCLOCKING',
		start: 1,
		max: 3,
		incr: 1,

		cost: {
			initial: 9000,
			ramp: 3 //Multiplier
		},

		color: '#0ff',
		init: function(level) {
			$r.hero.overclocking = (level - 1) / 2;
		},
		onBuy: function(level) {
			$r.hero.overclocking += 0.5;
		}
	}, {
		id: 'wdrive',
		title: 'WARP DRIVE',
		start: 0,
		max: 1,
		incr: 1,

		cost: {
			initial: 15000,
			ramp: 1 //Multiplier
		},

		color: '#fff',
		init: function(level) {
			$r.hero.warp = level ? true : false;
			$r.definitions.ActiveUpgrades.warp = level ? true : false;
		},
		onBuy: function(level) {
			$r.hero.warp = true;
			$r.definitions.ActiveUpgrades.warp = true;
		}
	}, {
		id: 'cannons',
		title: 'CANNON',
		start: 0,
		max: 5,
		incr: 1,

		cost: {
			initial: 2000,
			ramp: Math.E - .02 //Multiplier
		},

		color: '#71c0ef',
		init: function(level) {
			$r.hero.cannons = level;
		},
		onBuy: function(level) {
			$r.hero.cannons++;
		}
	}, {
		id: 'recharge',
		title: 'RECHARGE MODULE',
		start: 0,
		max: 1,
		incr: 1,

		cost: {
			initial: 100000,
			ramp: 1 //Multiplier
		},

		color: '#ff6868',
		init: function(level) {
			$r.hero.regen = level ? true : false;
		},
		onBuy: function() {
			$r.hero.regen = true;
		}
	}, {
		id: 'blink',
		title: 'BLINK MODULE',
		start: 0,
		max: 1,
		incr: 1,

		cost: {
			initial: 100000,
			ramp: 1 //Multiplier
		},

		color: '#0ff',
		init: function(level) {
			$r.hero.blink = level ? true : false;
		},
		onBuy: function() {
			$r.hero.blink = true;
		}
	}, {
	id: 'bigshot',
	title: 'SPLINTER MODULE',
	start: 0,
	max: 1,
	incr: 1,

	cost: {
		initial: 200000,
		ramp: 1 //Multiplier
	},

	color: '#f0f',
	init: function(level) {
		$r.hero.refract = level ? true : false;
	},
	onBuy: function() {
		$r.hero.refract = true;
	}
}
];

$r.definitions.ActiveUpgrades = [];

$r.getUpgradeById = function(id) {
	return $r.definitions.upgrades.find(function(e) {
		return e.id === id;
	});
}

$r.getUpgradeCost = function(id) {
	var upgrade = $r.getUpgradeById(id);
	var currentLevel = $r.storage.upgrades[id];
	if(currentLevel > upgrade.start) {
		return Math.floor(upgrade.cost.initial * Math.pow(upgrade.cost.ramp, currentLevel - upgrade.start))
	}

	return Math.floor(upgrade.cost.initial);
};

$r.getUpgradeAmt = function(id) {
	var upgr = $r.getUpgradeById(id);
	var amt = $r.storage.upgrades[id];
	return amt + '/' + upgr.max;
}

$r.isUpgradeMaxxed = function(id) {
	var upgr = $r.getUpgradeById(id);
	var amt = $r.storage.upgrades[id];

	return upgr.max == amt;
}

$r.getUpgradeLevel = function(id) {
	return $r.storage.upgrades[id] || 0;
}

$r.purchaseUpgrade = function(id) {
	var upgrade = $r.getUpgradeById(id);
	var cost = $r.getUpgradeCost(id);
	var cash = $r.storage['rp'];

	if($r.storage.upgrades[id] >= upgrade.max) {
		$r.storage.upgrades[id] = upgrade.max;
		return alert('You are at the maximum for that upgrade');
	}

	if(cost > cash) {
		return alert('This upgrade costs an additional ' + (cost - cash) + ' RP');
	}

	$r.storage.upgrades[id] += upgrade.incr;
	$r.addMoney(-cost);
	upgrade.onBuy($r.storage.upgrades[id]);
	$r.updateStorage();
}