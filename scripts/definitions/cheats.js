$r.definitions.cheats = {
	"moreMoney": {
		args: 1,
		effect: function(amt) {
			try {
				amt = parseInt(amt);
			} catch(ex) {
				return alert('Invalid amount! (Must be number)');
			}
			$r.addMoney(amt);
		}
	},
	"test": {
		effect: function() {
			alert("Cheating works!");
		}
	},
	"nuke": {
		ingame: true,
		effect: $r.definitions.nuke
	},
	"killAll": {
		ingame: true,
		effect: function() {
			var i = $r.enemies.length;
			while( i-- ) {
				var enemy = $r.enemies[i];
				
				enemy.receiveDamage(i, 10000);
			}
		}
	},
	"norris": {
		ingame: true,
		effect: function() {
			$r.hero.god =! $r.hero.god;
		}
	},
	"pi": {
		ingame: true,
		effect: function() {
			$r.hero.threeSixtyShot =! $r.hero.threeSixtyShot;
		}
	}
};


$r.definitions.cheats.handle = function() {
	var input = prompt("ENTER CODE", "");
		
	if(input != null && input != '') {
		var args = /(\w+)\[(.*)\]/.exec(input);

		if(args && args.length == 3) {
			input = args[1];
			args = args[2].split(/,\s*/g);
		}

		if(input in $r.definitions.cheats) {
			var cheat = $r.definitions.cheats[input];

			if(true) {
				if(cheat.args && cheat.args.length > (args ? args.length : 0))
					return alert('Invalid number of arguments!');

				cheat.effect.apply(cheat, args);
			} else {
				alert("That cheat can only be run in-game!");
			}
		} else {
			alert("Invalid cheat!");
		}
	}
}