$r.gunmods = [];

$r.gunmods.register = function() {
	if(arguments.length < 2) throw new Error('Two arguments required');
	var id = arguments[0];
	var handler = arguments[1];

	var init = arguments[2] || function() {};

	$r.gunmods.push({
		id: id,
		onFire: handler,
		init: init
	});

	init();
}

$r.gunmods.handle = function(bullet) {
	for(var x of $r.gunmods) {
		var out = x.onFire(bullet);

		if(out) bullet = out;
	}

	return bullet;
}