$r.definitions.weapons = {
	blaster: {
		size: 15,
		lineWidth: 2,
		damage: 1,
		speed: 10,
		piercing: 0,
		explosive: 0,
		fire: function(opts) {
			opts = $r.gunmods.handle(opts);

			var x = opts.x;
			var y = opts.y;
			var d = opts.direction;
			var c = opts.count;
			var s = opts.spread;
			var l = (opts.hasOwnProperty('colors') ? opts.colors : (opts.hasOwnProperty('color') ? [opts.color] : ['#fff']));
			var v = opts.speed || this.speed;
			var p = opts.damage || this.damage;

			var a = (opts.hasOwnProperty('abilities') ? opts.abilities : {});

			if( c > 1 ) {
				var spreadStart = -s / 2;
				var spreadStep = s / (c - 1);
			} else {
				var spreadStart = 0;
				var spreadStep = 0;
			}

			$r.audio.play( 'shoot' );
			if( $r.powerupTimers[ 2 ] > 0 || $r.powerupTimers[ 3 ] > 0 || $r.powerupTimers[ 4 ] > 0) $r.audio.play( 'shootAlt' );

			for(var i = 0; i < c; i++) {
				$r.bulletsFired++;
				var color = l[ Math.floor( $r.util.rand( 0, l.length ) ) ];

				var val = {
					x: x,
					y: y,
					speed: v,
					direction: d + spreadStart + i * spreadStep,
					damage: p,
					size: this.size,
					lineWidth: this.lineWidth,
					strokeStyle: color
				};

				for(var w in a) {
					val[w] = a[w];
				}

				var bullet = new $r.Bullet(val);
				$r.bullets.push(bullet);
			}
		}
	},
	laser: {
		size: 5000,
		lineWidth: 4,
		damage: 1,
		speed: 10,
		piercing: 1,
		strokeStyle: '#0ff',
		fire: function(opts) {
			var x = opts.x;
			var y = opts.y;
			var d = opts.direction;


		}
	}
}

for(var weaponid in $r.definitions.weapons) {
	var weapon = $r.definitions.weapons[weaponid];
	weapon._defaults = {};

	for(var opt in weapon) {
		if(typeof(weapon[opt]) !== 'function') {

		}
	}
}