/*==============================================================================
Init
==============================================================================*/
$r.Hero = function() {
	this.x = $r.ww / 2;
	this.y = $r.wh / 2;
	this.vx = 0;
	this.vy = 0;
	this.vmax = 6;
	this.direction = 0;
	this.accel = 0.5;
	this.radius = 10;
	this.life = 1;
	this.takingDamage = 0;
	this.fillStyle = '#fff';
	this.weapon = {
		type: 'blaster',
		fireRate: 5,
		fireRateTick: 5,
		spread: 0.3,
		count: 1,
		bullet: {
			size: 15,
			lineWidth: 2,
			damage: 1,
			speed: 10,
			piercing: 0,
			strokeStyle: '#fff'
		},
		fireFlag: 0
	};

	this.cannons = 0;
	
	this.god = 0;
	this.cheater = 0;
	this.threeSixtyShot = 0;

	this.maxHealth = 1;
	this.damageDampening = 1;
	this.dps = 1;
	this.overclocking = 0;
	this.warp = false;

	this.regen = false;
	this.shotgun = true;
};

$r.Hero.prototype.modVX = function() {
	return this.vmax + (this.warp ? 1000 : 0);
}

$r.Hero.prototype.modAccel = function() {
	return this.accel + (this.warp ? 0.25 : 0);
}

/*==============================================================================
Update
==============================================================================*/
$r.Hero.prototype.update = function() {
	if( this.life > 0 ) {
		if(this.regen) {
			if( this.life < this.maxHealth ) {
				this.life += 0.001;
			}
			if( $r.hero.life > this.maxHealth ) {
				this.life = this.maxHealth;
			}
		}

		/*==============================================================================
		Apply Forces
		==============================================================================*/
		if( $r.keys.state.up ) {
			this.vy -= this.modAccel() * $r.dt;
			if( this.vy < -(this.modVX()) ) {
				this.vy = -(this.modVX());
			}
		} else if( $r.keys.state.down ) {
			this.vy += this.modAccel() * $r.dt;
			if( this.vy > (this.modVX()) ) {
				this.vy = (this.modVX());
			}
		}
		if( $r.keys.state.left ) {
			this.vx -= this.modAccel() * $r.dt;
			if( this.vx < -(this.vmax + (this.warp * 4)) ) {
				this.vx = -(this.vmax + (this.warp * 4));
			}
		} else if( $r.keys.state.right ) {
			this.vx += this.modAccel() * $r.dt;
			if( this.vx > (this.vmax + (this.warp * 4)) ) {
				this.vx = (this.vmax + (this.warp * 4));
			}
		}

		this.vy *= 0.9;
		this.vx *= 0.9;	
		
		this.x += this.vx * $r.dt;
		this.y += this.vy * $r.dt;

		/*==============================================================================
		Lock Bounds
		==============================================================================*/
		if( this.x >= $r.ww - this.radius ) {
			this.x = $r.ww - this.radius;
		}
		if( this.x <= this.radius ) {
			this.x = this.radius;
		}
		if( this.y >= $r.wh - this.radius ) {
			this.y = $r.wh - this.radius;
		}
		if( this.y <= this.radius ) {
			this.y = this.radius;
		}

		/*==============================================================================
		Update Direction
		==============================================================================*/
		var dx = $r.mouse.x - this.x,
			dy = $r.mouse.y - this.y;
		this.direction = Math.atan2( dy, dx );

		if(this.threeSixtyShot) {
			this.weapon.spread = 12;
			this.weapon.count = 12;
		}

		/*==============================================================================
		Fire Weapon
		==============================================================================*/
		if( this.weapon.fireRateTick < (this.weapon.fireRate - this.overclocking) ){
			this.weapon.fireRateTick += $r.dt;
		} else {
			if( (!$r.hardMode && $r.autofire) || ( !(!$r.hardMode && $r.autofire) && $r.mouse.down ) ){
				var gunX = this.x + Math.cos( this.direction ) * ( this.radius + this.weapon.bullet.size );
				var gunY = this.y + Math.sin( this.direction ) * ( this.radius + this.weapon.bullet.size );

				var colors = [this.weapon.bullet.strokeStyle];
				if( $r.powerupTimers[ 2 ] > 0 || $r.powerupTimers[ 3 ] > 0 || $r.powerupTimers[ 4 ] > 0) {
					colors.length = 0;
					if( $r.powerupTimers[ 2 ] > 0 ) { colors.push( 'hsl(' + $r.definitions.powerups[ 2 ].hue + ', ' + $r.definitions.powerups[ 2 ].saturation + '%, ' + $r.definitions.powerups[ 2 ].lightness + '%)' ); }
					if( $r.powerupTimers[ 3 ] > 0 ) { colors.push( 'hsl(' + $r.definitions.powerups[ 3 ].hue + ', ' + $r.definitions.powerups[ 3 ].saturation + '%, ' + $r.definitions.powerups[ 3 ].lightness + '%)' ); }
					if( $r.powerupTimers[ 4 ] > 0 ) { colors.push( 'hsl(' + $r.definitions.powerups[ 4 ].hue + ', ' + $r.definitions.powerups[ 4 ].saturation + '%, ' + $r.definitions.powerups[ 4 ].lightness + '%)' ); }
					if( $r.powerupTimers[ 5 ] > 0 ) { colors.push( 'hsl(' + $r.definitions.powerups[ 5 ].hue + ', ' + $r.definitions.powerups[ 5 ].saturation + '%, ' + $r.definitions.powerups[ 5 ].lightness + '%)' ); }
				}

				$r.definitions.weapons[this.weapon.type].fire({
					x: gunX,
					y: gunY,
					direction: $r.hero.direction,
					count: $r.hero.weapon.count + $r.hero.cannons,
					spread: $r.hero.weapon.spread,
					colors: colors,
					speed: $r.hero.weapon.bullet.speed,
					damage: $r.hero.weapon.bullet.damage * $r.hero.dps,
					abilities: {
						piercing: $r.hero.weapon.bullet.piercing,
						explosive: $r.powerups.explosive
					}
				});

				this.weapon.fireRateTick = this.weapon.fireRateTick - (this.weapon.fireRate - this.overclocking);
				this.weapon.fireFlag = 6;

				//$r.audio.play( 'shoot' );
				//if( $r.powerupTimers[ 2 ] > 0 || $r.powerupTimers[ 3 ] > 0 || $r.powerupTimers[ 4 ] > 0) {
				//	$r.audio.play( 'shootAlt' );
				//}
				//
				//this.weapon.fireRateTick = this.weapon.fireRateTick - (this.weapon.fireRate - this.overclocking);
				//this.weapon.fireFlag = 6;
				//
				//if( this.weapon.count + this.cannons > 1 ) {
				//	var spreadStart = -this.weapon.spread / 2;
				//	var spreadStep = this.weapon.spread / ( this.cannons + this.weapon.count - 1 );
				//} else {
				//	var spreadStart = 0;
				//	var spreadStep = 0;
				//}
				//
				//
				//for( var i = 0; i < this.weapon.count + this.cannons; i++ ) {
				//	$r.bulletsFired++;
				//	var color = this.weapon.bullet.strokeStyle;
				//	if( $r.powerupTimers[ 2 ] > 0 || $r.powerupTimers[ 3 ] > 0 || $r.powerupTimers[ 4 ] > 0) {
				//		var colors = [];
				//		if( $r.powerupTimers[ 2 ] > 0 ) { colors.push( 'hsl(' + $r.definitions.powerups[ 2 ].hue + ', ' + $r.definitions.powerups[ 2 ].saturation + '%, ' + $r.definitions.powerups[ 2 ].lightness + '%)' ); }
				//		if( $r.powerupTimers[ 3 ] > 0 ) { colors.push( 'hsl(' + $r.definitions.powerups[ 3 ].hue + ', ' + $r.definitions.powerups[ 3 ].saturation + '%, ' + $r.definitions.powerups[ 3 ].lightness + '%)' ); }
				//		if( $r.powerupTimers[ 4 ] > 0 ) { colors.push( 'hsl(' + $r.definitions.powerups[ 4 ].hue + ', ' + $r.definitions.powerups[ 4 ].saturation + '%, ' + $r.definitions.powerups[ 4 ].lightness + '%)' ); }
				//		if( $r.powerupTimers[ 5 ] > 0 ) { colors.push( 'hsl(' + $r.definitions.powerups[ 5 ].hue + ', ' + $r.definitions.powerups[ 5 ].saturation + '%, ' + $r.definitions.powerups[ 5 ].lightness + '%)' ); }
				//		color = colors[ Math.floor( $r.util.rand( 0, colors.length ) ) ];
				//	}
				//
				//	var bullet = new $r.Bullet({
				//		x: gunX,
				//		y: gunY,
				//		speed: this.weapon.bullet.speed,
				//		direction: this.direction + spreadStart + i * spreadStep,
				//		damage: this.weapon.bullet.damage * this.dps,
				//		size: this.weapon.bullet.size,
				//		lineWidth: this.weapon.bullet.lineWidth,
				//		strokeStyle: color,
				//		piercing: this.weapon.bullet.piercing,
				//		explosive: $r.powerups.explosive
				//	});
				//
				//	if(this.shotgun)
				//		bullet.lifespan = 400;
				//
				//	$r.bullets.push(bullet);
				//}
			}
		}

		/*==============================================================================
		Check Collisions
		==============================================================================*/
		this.takingDamage = 0;
		var ei = $r.enemies.length;
		while( ei-- ) {
			var enemy = $r.enemies[ ei ];
			if( enemy.inView && $r.util.distance( this.x, this.y, enemy.x, enemy.y ) <= this.radius + enemy.radius ) {
				$r.particleEmitters.push( new $r.ParticleEmitter( {
					x: this.x,
					y: this.y,
					count: 2,
					spawnRange: 0,
					friction: 0.85,
					minSpeed: 2,
					maxSpeed: 15,
					minDirection: 0,
					maxDirection: $r.twopi,
					hue: 0,
					saturation: 0
				} ) );
				this.takingDamage = 1;
				this.life -= this.god ? 0 : (0.0075 / this.damageDampening);
				$r.rumble.level = 3;
				if( Math.floor( $r.tick ) % 5 == 0 ){
					$r.audio.play( 'takingDamage' );
				}
			}
		}		
	}
};

/*==============================================================================
Render
==============================================================================*/
$r.Hero.prototype.render = function() {
	if( this.life > 0 ) {
		if( this.takingDamage ) {
			var fillStyle = 'hsla(0, 0%, ' + $r.util.rand( 0, 100 ) + '%, 1)';
			$r.ctxmg.fillStyle = 'hsla(0, 0%, ' + $r.util.rand( 0, 100 ) + '%, ' + $r.util.rand( 0.01, 0.15 ) + ')';
			$r.ctxmg.fillRect( -$r.screen.x, -$r.screen.y, $r.cw, $r.ch );
		} else if( this.weapon.fireFlag > 0 ) {
			this.weapon.fireFlag -= $r.dt;
			var fillStyle = 'hsla(' + $r.util.rand( 0, 359 ) + ', 100%, ' + $r.util.rand( 20, 80 ) + '%, 1)';
		} else {
			var fillStyle = this.fillStyle;
		}

		$r.ctxmg.save();
		$r.ctxmg.translate( this.x, this.y );
		$r.ctxmg.rotate( this.direction - $r.pi / 4 );
		$r.ctxmg.fillStyle = fillStyle;
		$r.ctxmg.fillRect( 0, 0, this.radius, this.radius );
		$r.ctxmg.restore();

		$r.ctxmg.save();
		$r.ctxmg.translate( this.x, this.y );
		$r.ctxmg.rotate( this.direction - $r.pi / 4 + $r.twopi / 3 );
		$r.ctxmg.fillStyle = fillStyle;
		$r.ctxmg.fillRect( 0, 0, this.radius, this.radius );
		$r.ctxmg.restore();

		$r.ctxmg.save();
		$r.ctxmg.translate( this.x, this.y );
		$r.ctxmg.rotate( this.direction - $r.pi / 4 - $r.twopi / 3 );
		$r.ctxmg.fillStyle = fillStyle;
		$r.ctxmg.fillRect( 0, 0, this.radius, this.radius );
		$r.ctxmg.restore();

		$r.util.fillCircle( $r.ctxmg, this.x, this.y, this.radius - 3, fillStyle );
	}	
};