$r.Bullet = function( opt ) {
	for( var k in opt ) {
		this[k] = opt[k];
	}

	if(!this.type) this.type = 'bullet';
	if(!this.lifespan) this.lifespan = 500;

	this.enemiesHit = [];
	this.inView = 0;
	this.life = 0;

	$r.particleEmitters.push( new $r.ParticleEmitter( {
		x: this.x,
		y: this.y,
		count: 1,
		spawnRange: 1,
		friction: 0.75,
		minSpeed: 2,
		maxSpeed: 10,
		minDirection: 0,
		maxDirection: $r.twopi,
		hue: 0,
		saturation: 0
	} ) );
};

$r.Bullet.prototype.update = function( i ) {
	if(this.life > this.lifespan)
		return $r.bullets.splice( i, 1 );

	if($r.powerups.homing) {
		
	} else {
		this.x += Math.cos( this.direction ) * ( this.speed * $r.dt );
		this.y += Math.sin( this.direction ) * ( this.speed * $r.dt );
		this.ex = this.x - Math.cos( this.direction ) * this.size;
		this.ey = this.y - Math.sin( this.direction ) * this.size;
	}

	var ei = $r.enemies.length;
	while( ei-- ) {
		var enemy = $r.enemies[ ei ];
		var shotRadius = ($r.powerups.explosive) ? 80 : enemy.radius;
		//var shotRadius = enemy.radius; //For now, leave it as-is... It's buggy!
		
		if( $r.util.distance( this.x, this.y, enemy.x, enemy.y ) <=  shotRadius) {
			if( this.enemiesHit.indexOf( enemy.index ) == -1 ){
				$r.particleEmitters.push( new $r.ParticleEmitter( {
					x: this.x,
					y: this.y,
					count: Math.floor( $r.util.rand( 1, 4 ) ),
					spawnRange: 0,
					friction: 0.85,
					minSpeed: 5,
					maxSpeed: 12,
					minDirection: ( this.direction - $r.pi ) - $r.pi / 5,
					maxDirection: ( this.direction - $r.pi ) + $r.pi / 5,
					hue: enemy.hue
				} ) );

				this.enemiesHit.push( enemy.index );
				enemy.receiveDamage( ei, this.damage );
			}

			if( !this.piercing ) {
				if(!this.explosive || this.enemiesHit > 3) {
					$r.bullets.splice( i, 1 );
				}
			}
		}
	}

	if( !$r.util.pointInRect( this.ex, this.ey, 0, 0, $r.ww, $r.wh ) ) {
		$r.bullets.splice( i, 1 );
	}

	if( $r.util.pointInRect( this.ex, this.ey, -$r.screen.x, -$r.screen.y, $r.cw, $r.ch ) ) {
		this.inView = 1;
	} else {
		this.inView = 0;
	}

	this.life++;
};

$r.Bullet.prototype.render = function( i ) {
	if( this.inView ) {
		$r.ctxmg.beginPath();
		$r.ctxmg.moveTo( this.x, this.y );
		$r.ctxmg.lineTo( this.ex, this.ey );
		$r.ctxmg.lineWidth = this.lineWidth;
		$r.ctxmg.strokeStyle = this.strokeStyle;
		$r.ctxmg.stroke();
	}
};