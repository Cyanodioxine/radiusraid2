$r.util.keys = {}
$r.util.rkeys = [];

for(var i = 65; i <= 90; i++) {
	var key = String.fromCharCode(i);

	$r.util.keys[key.toLowerCase()] = i;
	$r.util.keys[key] = i;
}
$r.util.keys['tab'] = 9;

$r.util.keys['escape'] = 27;
$r.util.keys['space'] = 32;

$r.util.keys['shift'] = 16;
$r.util.keys['ctrl'] = 17;
$r.util.keys['alt'] = 18;

$r.util.keys['left'] = 37;
$r.util.keys['up'] = 38;
$r.util.keys['right'] = 39;
$r.util.keys['down'] = 40;

$r.util.keys['tilde'] = 192;

for(var i = 96; i <= 103; i++) {
	var key = 'num' + (i - 96);
	$r.util.keys[key] = i;
}

for(var key in $r.util.keys) {
	var code = $r.util.keys[key];
	if(!$r.util.rkeys[code])
		$r.util.rkeys[code] = key;
}

$r.util.kfc = function(keycode) {
	return $r.util.rkeys[keycode] || '';
}

$r.util.cfk = function(key) {
	return $r.util.keys[keycode] || -1;
}