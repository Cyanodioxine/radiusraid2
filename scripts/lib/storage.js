Storage.prototype.setObject = function( key, value ) {
	this.setItem( key, JSON.stringify( value ) );
}

Storage.prototype.getObject = function( key ) {
	var value = this.getItem( key );
	return value && JSON.parse( value );
}

Storage.prototype.removeObject = function( key ) {
	this.removeItem( key );
}

$r.incrementStorage = function( key, amount) {
	$r.storage[key] += amount;
	$r.updateStorage();
}

$r.setupStorage = function() {
	$r.storage = {
		mute: 0,
		autofire: 0,
		score: 0,
		level: 0,
		rounds: 0,
		kills: 0,
		bullets: 0,
		powerups: 0,
		time: 0,
		rp: 0,
		upgrades: { },
		controls: {
			left: 'a',
			right: 'd',
			up: 'w',
			down: 's',
			fire: 'mouse0',
			autofire: 'f',
			pause: 'esc',
			blink: 'space'
		},
		showMinimap: 1,
		hard: 0
	};

	console.log('Pulling ' + $r.definitions.upgrades.length + ' upgrades');

	for(var i in $r.definitions.upgrades) {
		var upgrade = $r.definitions.upgrades[i];
		$r.storage.upgrades[upgrade.id] = upgrade.start;
	}

	var db = localStorage.getObject( 'radiusraid' );

	if(db) {
		for(var x in $r.storage) {
			var a = $r.storage[x];

			if(a.constructor === {}.constructor) {
				for(var y in a) {
					if(db[x] !== null && db[x][y] !== null && typeof(db[x][y]) !== 'undefined')
						$r.storage[x][y] = db[x][y];
				}
			} else {
				if(db[x] !== null);
				$r.storage[x] = db[x];
			}
		}
	}


	$r.updateStorage();
};


$r.addMoney = function(amt) {
	$r.storage['rp'] += amt;

	var color = (amt > 0) ? '#0d0' : '#f00';

	if($r.state == 'play') {
		$r.ctxmg.beginPath();
		$r.textPops.push( new $r.TextPop( {
			x: 400,
			y: 570,
			value: amt + ' RP',
			color: color,
			prefix: amt > 0 ? '+' : '-',
			snap: 1,
			overlay: 1,
			alpha: 1
		} ) );
	}

	$r.updateStorage();
}

$r.setStorageValue = function(id, val) {
	var ids = id.split(/\./g);

	if(ids.length > 1) {
		var path = $r.storage;

		for(var i = 0; i < ids.length; i++) {
			var ix = ids[i];
			if(i + 1 == ids.length) {
				path[ix] = val;
				return $r.updateStorage();
			}

			if(!path[ix]) {
				path[ix] = {}
				path = path[ix];
			}
		}
		$r.updateStorage();
	} else {
		$r.storage[id] = val;
		$r.updateStorage();
	}
}

$r.getFromStorage = function(id) {
	var ids = id.split(/\./g);

	if(ids.length > 1) {
		var path = $r.storage;

		for(var i = 0; i < ids.length; i++) {
			var ix = ids[i];
			if(i + 1 == ids.length) {
				return path[ix];
			}

			if(path[ix]) {
				path = path[ix];
			} else return null;
		}
	} else {
		return $r.storage[id];
	}
}

$r.updateStorage = function() {
	localStorage.setObject( 'radiusraid', $r.storage );
};

$r.clearStorage = function() {
	localStorage.removeObject( 'radiusraid' );
	$r.setupStorage();
};