$r.mousemovecb = function( e ) {
	e.preventDefault();
	$r.mouse.ax = e.pageX;
	$r.mouse.ay = e.pageY;
	$r.mousescreen();
};

$r.mousescreen = function() {
	$r.mouse.sx = $r.mouse.ax - $r.cOffset.left;
	$r.mouse.sy = $r.mouse.ay - $r.cOffset.top;
	$r.mouse.x = $r.mouse.sx - $r.screen.x;
	$r.mouse.y = $r.mouse.sy - $r.screen.y;
};

$r.mousedowncb = function( e ) {
	e.preventDefault();
	$r.mouse.down = 1;
};

$r.mouseupcb = function( e ) {
	e.preventDefault();
	$r.mouse.down = 0;
};

$r.keydowncb = function( e ) {
	var e = ( e.keyCode ? e.keyCode : e.which );
	if( e === 38 || e === 87 ){ $r.keys.state.up = 1; }
	if( e === 39 || e === 68 ){ $r.keys.state.right = 1; }
	if( e === 40 || e === 83 ){ $r.keys.state.down = 1; }
	if( e === 37 || e === 65 ){ $r.keys.state.left = 1; }
	if( e === 70 ){ $r.keys.state.f = 1; }
	if( e === 77 ){ $r.keys.state.m = 1; }
	if( e === 27 ){ $r.keys.state.p = 1; }

	if(e === 32){$r.keys.state.space = 1}

	if(e === 67)
	{
		$r.definitions.cheats.handle();
	}
}

$r.keyupcb = function( e ) {
	var e = ( e.keyCode ? e.keyCode : e.which );
	if( e === 38 || e === 87 ){ $r.keys.state.up = 0; }
	if( e === 39 || e === 68 ){ $r.keys.state.right = 0; }
	if( e === 40 || e === 83 ){ $r.keys.state.down = 0; }
	if( e === 37 || e === 65 ){ $r.keys.state.left = 0; }
	if( e === 67){$r.keys.state.c = 0;}
	if( e === 70 ){ $r.keys.state.f = 0; }
	if( e === 77 ){ $r.keys.state.m = 0; }
	if( e === 27 ){ $r.keys.state.p = 0; }
	if(e === 32){$r.keys.state.space = 0}
}

$r.resizecb = function( e ) {
	var rect = $r.cmg.getBoundingClientRect();
	$r.cOffset = {
		left: rect.left,
		top: rect.top
	}
}

$r.blurcb = function() {
	if( $r.state == 'play' ){
		$r.setState( 'pause' );
	}
}

$r.bindEvents = function() {
	window.addEventListener( 'mousemove', $r.mousemovecb );
	window.addEventListener( 'mousedown', $r.mousedowncb );
	window.addEventListener( 'mouseup', $r.mouseupcb );
	window.addEventListener( 'keydown', $r.keydowncb );
	window.addEventListener( 'keyup', $r.keyupcb );
	window.addEventListener( 'resize', $r.resizecb );
	window.addEventListener( 'blur', $r.blurcb );
};