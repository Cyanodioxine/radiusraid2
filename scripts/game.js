/*==============================================================================
 Init
 ==============================================================================*/
$r.init = function() {
	$r.wrap = document.getElementById( 'wrap' );
	$r.wrapInner = document.getElementById( 'wrap-inner' );
	$r.cbg1 = document.getElementById( 'cbg1' );
	$r.cbg2 = document.getElementById( 'cbg2' );
	$r.cbg3 = document.getElementById( 'cbg3' );
	$r.cbg4 = document.getElementById( 'cbg4' );
	$r.cmg = document.getElementById( 'cmg' );
	$r.cfg = document.getElementById( 'cfg' );
	$r.ctxbg1 = $r.cbg1.getContext( '2d' );
	$r.ctxbg2 = $r.cbg2.getContext( '2d' );
	$r.ctxbg3 = $r.cbg3.getContext( '2d' );
	$r.ctxbg4 = $r.cbg4.getContext( '2d' );
	$r.ctxmg = $r.cmg.getContext( '2d' );
	$r.ctxfg = $r.cfg.getContext( '2d' );

	$r.cw = $r.cmg.width = $r.cfg.width = 800;
	$r.ch = $r.cmg.height = $r.cfg.height = 600;

	$r.wrap.style.width = $r.wrapInner.style.width = $r.cw + 'px';
	$r.wrap.style.height = $r.wrapInner.style.height = $r.ch + 'px';
	//$r.wrap.style.marginLeft = ( -$r.cw / 2 ) - 10 + 'px';
	//$r.wrap.style.marginTop = ( -$r.ch / 2 ) - 10 + 'px';
	$r.ww = Math.floor( $r.cw * 2 );
	$r.wh = Math.floor( $r.ch * 2 );
	$r.cbg1.width = Math.floor( $r.cw * 1.1 );
	$r.cbg1.height = Math.floor( $r.ch * 1.1 );
	$r.cbg2.width = Math.floor( $r.cw * 1.15 );
	$r.cbg2.height = Math.floor( $r.ch * 1.15 );
	$r.cbg3.width = Math.floor( $r.cw * 1.2 );
	$r.cbg3.height = Math.floor( $r.ch * 1.2 );
	$r.cbg4.width = Math.floor( $r.cw * 1.25 );
	$r.cbg4.height = Math.floor( $r.ch * 1.25 );
	$r.emp = 0;

	$r.screen = {
		x: ( $r.ww - $r.cw ) / -2,
		y: ( $r.wh - $r.ch ) / -2
	};

	$r.setupStorage();

	$r.mute = $r.storage['mute'];
	$r.autofire = $r.storage['autofire'];
	$r.slowEnemyDivider = 3;

	$r.hardMode = $r.storage['hard'];

	$r.keys = {
		state: {
			up: 0,
			down: 0,
			left: 0,
			right: 0,
			f: 0,
			m: 0,
			p: 0,
			space: 0
		},
		pressed: {
			up: 0,
			down: 0,
			left: 0,
			right: 0,
			f: 0,
			m: 0,
			p: 0,
			space: 0
		}
	};
	$r.okeys = {};
	$r.mouse = {
		x: $r.ww / 2,
		y: $r.wh / 2,
		sx: 0,
		sy: 0,
		ax: window.innerWidth / 2,
		ay: 0,
		down: 0
	};
	$r.buttons = [];

	$r.minimap = {
		x: 20,
		y: $r.ch - Math.floor( $r.ch * 0.15 ) - 20,
		width: Math.floor( $r.cw * 0.15 ),
		height: Math.floor( $r.ch * 0.15 ),
		scale: Math.floor( $r.cw * 0.15 ) / $r.ww,
		color: 'hsla(0, 0%, 0%, 0.65)',
		strokeColor: '#3a3a3a',
		visible: $r.storage['showMinimap'] ? true : false
	},
		$r.cOffset = {
			left: 0,
			top: 0
		};

	$r.levelCount = $r.definitions.levels.length;
	$r.state = '';
	$r.enemies = [];
	$r.bullets = [];
	$r.explosions = [];
	$r.powerups = [];
	$r.particleEmitters = [];
	$r.textPops = [];
	$r.levelPops = [];
	$r.powerupTimers = [];

	loadInterfaces();

	$r.levelUI = new $r.LevelPop();

	$r.resizecb();
	$r.bindEvents();
	$r.setupStates();
	$r.renderBackground1();
	$r.renderBackground2();
	$r.renderBackground3();
	$r.renderBackground4();
	$r.renderForeground();
	$r.renderFavicon();
	$r.setState( 'menu' );

	$r.loop();
};

/*==============================================================================
 Reset
 ==============================================================================*/
$r.reset = function() {
	$r.indexGlobal = 0;
	$r.dt = 1;
	$r.lt = 0;
	$r.elapsed = 0;
	$r.tick = 0;

	$r.gameoverTick = 0;
	$r.gameoverTickMax = 200;
	$r.gameoverExplosion = 0;

	$r.instructionTick = 0;
	$r.instructionTickMax = 400;

	$r.levelDiffOffset = 0;
	$r.enemyOffsetMod = 0;
	$r.slow = 0;

	$r.screen = {
		x: ( $r.ww - $r.cw ) / -2,
		y: ( $r.wh - $r.ch ) / -2
	};
	$r.rumble = {
		x: 0,
		y: 0,
		level: 0,
		decay: 0.4
	};

	$r.mouse.down = 0;

	$r.level = {
		current: 0,
		kills: 0,
		killsToLevel: $r.definitions.levels[ 0 ].killsToLevel,
		distribution: $r.definitions.levels[ 0 ].distribution,
		distributionCount: $r.definitions.levels[ 0 ].distribution.length
	};

	$r.enemies.length = 0;
	$r.bullets.length = 0;
	$r.explosions.length = 0;
	$r.powerups.length = 0;
	$r.particleEmitters.length = 0;
	$r.textPops.length = 0;
	$r.powerupTimers.length = 0;

	for( var i = 0; i < $r.definitions.powerups.length; i++ ) {
		$r.powerupTimers.push( 0 );
	}

	$r.kills = 0;
	$r.bulletsFired = 0;
	$r.powerupsCollected = 0;
	$r.score = 0;

	$r.hero = new $r.Hero();

	$r.powerups.homing = 0;
	$r.powerups.explosive = 0;

	for(var i = 0; i < $r.definitions.upgrades.length; i++) {
		var upgrade = $r.definitions.upgrades[i];

		var level = $r.storage.upgrades[upgrade.id];
		upgrade.init(level);
	}
};

/*==============================================================================
 Render Foreground
 ==============================================================================*/
$r.renderForeground = function() {
	var gradient = $r.ctxfg.createRadialGradient( $r.cw / 2, $r.ch / 2, $r.ch / 3, $r.cw / 2, $r.ch / 2, $r.ch );
	gradient.addColorStop( 0, 'hsla(0, ' + ($r.hardMode * 100) + '%, 0%, 0)' );
	gradient.addColorStop( 1, 'hsla(0, ' + ($r.hardMode * 100) + '%, 0%, 0.5)' );
	$r.ctxfg.fillStyle = gradient;
	$r.ctxfg.fillRect( 0, 0, $r.cw, $r.ch );

	$r.ctxfg.fillStyle = 'hsla(0, ' + ($r.hardMode * 100) + '%, 50%, 0.1)';
	var i = (0.5 + ( $r.ch / 2 )) | 0;
	while( i-- ) {
		$r.ctxfg.fillRect( 0, i * 2, $r.cw, 1 );
	}

	var gradient2 = $r.ctxfg.createLinearGradient( $r.cw, 0, 0, $r.ch );
	gradient2.addColorStop( 0, 'hsla(0, ' + ($r.hardMode * 100) + '%, 100%, 0.04)' );
	gradient2.addColorStop( 0.75, 'hsla(0, ' + ($r.hardMode * 100) + '%, 100%, 0)' );
	$r.ctxfg.beginPath();
	$r.ctxfg.moveTo( 0, 0 );
	$r.ctxfg.lineTo( $r.cw, 0 );
	$r.ctxfg.lineTo( 0, $r.ch );
	$r.ctxfg.closePath();
	$r.ctxfg.fillStyle = gradient2;
	$r.ctxfg.fill();
}

/*==============================================================================
 Enemy Spawning
 ==============================================================================*/
$r.getSpawnCoordinates = function( radius ) {
	var quadrant = Math.floor( $r.util.rand( 0, 4 ) ),
		x,
		y,
		start;

	if( quadrant === 0){
		x = $r.util.rand( 0, $r.ww );
		y = -radius;
		start = 'top';
	} else if( quadrant === 1 ){
		x = $r.ww + radius;
		y = $r.util.rand( 0, $r.wh );
		start = 'right';
	} else if( quadrant === 2 ) {
		x = $r.util.rand( 0, $r.ww );
		y = $r.wh + radius;
		start = 'bottom';
	} else {
		x = -radius;
		y = $r.util.rand( 0, $r.wh );
		start = 'left';
	}

	return { x: x, y: y, start: start };
};

/*==============================================================================
 Miscellaneous
 ==============================================================================*/
$r.clearScreen = function() {
	$r.ctxmg.clearRect( 0, 0, $r.cw, $r.ch );
};

$r.updateDelta = function() {
	var now = Date.now();
	$r.dt = ( now - $r.lt ) / ( 1000 / 60 );
	$r.dt = ( $r.dt < 0 ) ? 0.001 : $r.dt;
	$r.dt = ( $r.dt > 10 ) ? 10 : $r.dt;
	$r.lt = now;
	$r.elapsed += $r.dt;
};

$r.updateScreen = function() {
	var xSnap,
		xModify,
		ySnap,
		yModify;

	if( $r.hero.x < $r.cw / 2 ) {
		xModify = $r.hero.x / $r.cw;
	} else if( $r.hero.x > $r.ww - $r.cw / 2 ) {
		xModify = 1 - ( $r.ww - $r.hero.x ) / $r.cw;
	} else {
		xModify = 0.5;
	}

	if( $r.hero.y < $r.ch / 2 ) {
		yModify = $r.hero.y / $r.ch;
	} else if( $r.hero.y > $r.wh - $r.ch / 2 ) {
		yModify = 1 - ( $r.wh - $r.hero.y ) / $r.ch;
	} else {
		yModify = 0.5;
	}

	xSnap = ( ( $r.cw * xModify - $r.hero.x ) - $r.screen.x ) / 30;
	ySnap = ( ( $r.ch * yModify - $r.hero.y ) - $r.screen.y ) / 30;

	// ease to new coordinates
	$r.screen.x += xSnap * $r.dt;
	$r.screen.y += ySnap * $r.dt;

	// update rumble levels, keep X and Y changes consistent, apply rumble
	if( $r.rumble.level > 0 ) {
		$r.rumble.level -= $r.rumble.decay;
		$r.rumble.level = ( $r.rumble.level < 0 ) ? 0 : $r.rumble.level;
		$r.rumble.x = $r.util.rand( -$r.rumble.level, $r.rumble.level );
		$r.rumble.y = $r.util.rand( -$r.rumble.level, $r.rumble.level );
	} else {
		$r.rumble.x = 0;
		$r.rumble.y = 0;
	}

	//$r.screen.x -= $r.rumble.x;
	//$r.screen.y -= $r.rumble.y;

	// animate background canvas
	$r.cbg1.style.marginLeft =
		-( ( $r.cbg1.width - $r.cw ) / 2 ) // half the difference from bg to viewport
		- ( ( $r.cbg1.width - $r.cw ) / 2 ) // half the diff again, modified by a percentage below
		  * ( ( -$r.screen.x - ( $r.ww - $r.cw ) / 2 ) / ( ( $r.ww - $r.cw ) / 2) ) // viewport offset applied to bg
		- $r.rumble.x + 'px';
	$r.cbg1.style.marginTop =
		-( ( $r.cbg1.height - $r.ch ) / 2 )
		- ( ( $r.cbg1.height - $r.ch ) / 2 )
		  * ( ( -$r.screen.y - ( $r.wh - $r.ch ) / 2 ) / ( ( $r.wh - $r.ch ) / 2) )
		- $r.rumble.y + 'px';
	$r.cbg2.style.marginLeft =
		-( ( $r.cbg2.width - $r.cw ) / 2 ) // half the difference from bg to viewport
		- ( ( $r.cbg2.width - $r.cw ) / 2 ) // half the diff again, modified by a percentage below
		  * ( ( -$r.screen.x - ( $r.ww - $r.cw ) / 2 ) / ( ( $r.ww - $r.cw ) / 2) ) // viewport offset applied to bg
		- $r.rumble.x + 'px';
	$r.cbg2.style.marginTop =
		-( ( $r.cbg2.height - $r.ch ) / 2 )
		- ( ( $r.cbg2.height - $r.ch ) / 2 )
		  * ( ( -$r.screen.y - ( $r.wh - $r.ch ) / 2 ) / ( ( $r.wh - $r.ch ) / 2) )
		- $r.rumble.y + 'px';
	$r.cbg3.style.marginLeft =
		-( ( $r.cbg3.width - $r.cw ) / 2 ) // half the difference from bg to viewport
		- ( ( $r.cbg3.width - $r.cw ) / 2 ) // half the diff again, modified by a percentage below
		  * ( ( -$r.screen.x - ( $r.ww - $r.cw ) / 2 ) / ( ( $r.ww - $r.cw ) / 2) ) // viewport offset applied to bg
		- $r.rumble.x + 'px';
	$r.cbg3.style.marginTop =
		-( ( $r.cbg3.height - $r.ch ) / 2 )
		- ( ( $r.cbg3.height - $r.ch ) / 2 )
		  * ( ( -$r.screen.y - ( $r.wh - $r.ch ) / 2 ) / ( ( $r.wh - $r.ch ) / 2) )
		- $r.rumble.y + 'px';
	$r.cbg4.style.marginLeft =
		-( ( $r.cbg4.width - $r.cw ) / 2 ) // half the difference from bg to viewport
		- ( ( $r.cbg4.width - $r.cw ) / 2 ) // half the diff again, modified by a percentage below
		  * ( ( -$r.screen.x - ( $r.ww - $r.cw ) / 2 ) / ( ( $r.ww - $r.cw ) / 2) ) // viewport offset applied to bg
		- $r.rumble.x + 'px';
	$r.cbg4.style.marginTop =
		-( ( $r.cbg4.height - $r.ch ) / 2 )
		- ( ( $r.cbg4.height - $r.ch ) / 2 )
		  * ( ( -$r.screen.y - ( $r.wh - $r.ch ) / 2 ) / ( ( $r.wh - $r.ch ) / 2) )
		- $r.rumble.y + 'px';

	$r.mousescreen();
};

$r.updatePowerupTimers = function()
{
	for(var i = 0; i < $r.definitions.powerups.length; i++)
	{
		var powerup = $r.definitions.powerups[i];

		if($r.powerupTimers[i] > 0) {
			if(!powerup.active) {
				powerup.active = 1;
			}

			$r.powerupTimers[ i ] -= $r.dt;
			powerup.whileActive();
		}
		else {
			if(powerup.active) {
				powerup.active = 0;
				if(powerup.onDisable)
					powerup.onDisable();
			}
		}
	}
};

$r.toggleHardMode = function(on) {
	$r.hardMode = (arguments.length >= 1 ? on : !$r.hardMode);

	$r.ctxfg.beginPath();
	$r.ctxfg.clearRect(0, 0, $r.cfg.width, $r.cfg.height);
	$r.ctxfg.stroke();
	$r.renderForeground();

	$r.ctxbg4.beginPath();
	$r.ctxbg4.clearRect(0, 0, $r.cbg4.width, $r.cbg4.height);
	$r.ctxbg4.stroke();
	$r.renderBackground4();

	$r.definitions.levels.repopulate();
}

/*==============================================================================
 Loop
 ==============================================================================*/
$r.loop = function() {
	requestAnimFrame( $r.loop );

	// setup the pressed state for all keys
	for( var k in $r.keys.state ) {
		if( $r.keys.state[ k ] && !$r.okeys[ k ] ) {
			$r.keys.pressed[ k ] = 1;
		} else {
			$r.keys.pressed[ k ] = 0;
		}
	}

	// run the current state
	$r.states[ $r.state ]();

	// always listen for mute toggle
	if( $r.keys.pressed.m ){
		$r.mute = ~~!$r.mute;
		var i = $r.audio.references.length;
		while( i-- ) {
			$r.audio.references[ i ].volume = ~~!$r.mute;
		}
		$r.storage['mute'] = $r.mute;
		$r.updateStorage();
	}

	// move current keys into old keys
	$r.okeys = {};
	for( var k in $r.keys.state ) {
		$r.okeys[ k ] = $r.keys.state[ k ];
	}
};