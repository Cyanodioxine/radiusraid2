$r = RadiusRaid = {};

$r.bootstrap = function(source) {
	if(Array.isArray(source)) {
		return new Promise(function (resolve) {
			var sources = source;
			var i = 0;

			function loop() {
				if(i == sources.length) {
					console.log('Loaded ' + source.length + ' scripts.');
					return resolve();
				}

				var scr = document.createElement('script');
				scr.setAttribute('src', sources[i]);

				scr.onload = function() {
					loop();
				};

				i++;
				document.head.appendChild(scr);
			}

			loop();
		});
	} else {
		var scr = document.createElement('script');
		scr.setAttribute('src', source);
		document.head.appendChild(scr);
	}
}

$(function() {
	var loadingSpinner = $('<div id="loadcover"><i id="loadicon" class="icon fa fa-refresh fa-spin"></i></div>');

	loadingSpinner.appendTo("#wrap");

    console.log("Bootstrapping RadiusRaid...");

	RadiusRaid.bootstrap([
		"scripts/lib/jsfxr.js",
		"scripts/lib/util.js",
		"scripts/lib/keys.js",
		"scripts/lib/storage.js",

		"scripts/definitions/definitions.js",
		"scripts/definitions/controls.js",
		"scripts/definitions/audio.js",
		"scripts/definitions/enemies.js",
		"scripts/definitions/levels.js",
		"scripts/definitions/powerups.js",
		"scripts/definitions/upgrades.js",
		"scripts/definitions/weapons.js",
		"scripts/definitions/cheats.js",

		"scripts/definitions/weapon/gunmods.js",

		"scripts/lib/audio.js",

		"scripts/ui/text.js",

		"scripts/entity/hero.js",
		"scripts/entity/enemy.js",
		"scripts/entity/bullet.js",
		"scripts/entity/explosion.js",
		"scripts/entity/powerup.js",
		"scripts/entity/particle.js",

		"scripts/world/particleemitter.js",
		"scripts/world/textpop.js",
		"scripts/world/levelpop.js",

		"scripts/ui/button.js",
		"scripts/cheat.js",

		"scripts/interface/interfaces.js",
		"scripts/interface/health.js",
		"scripts/interface/minimap.js",
		"scripts/interface/progress.js",
		"scripts/interface/score.js",

		"scripts/states/states.js",
		"scripts/states/statemanager.js",

		'scripts/states/state/menu.js',
		'scripts/states/state/stats.js',
		'scripts/states/state/credits.js',
		'scripts/states/state/shop.js',
		'scripts/states/state/settings.js',
		'scripts/states/state/game.js',
		'scripts/states/state/gameover.js',
		'scripts/states/state/pause.js',

		"scripts/lib/events.js",

		"scripts/ui/favicon.js",
		"scripts/world/background.js",

		"scripts/game.js",
	]).then(function() {
		console.log('Done. Let\'s begin!');

		loadingSpinner.fadeOut(500);
		setTimeout(function() {loadingSpinner.remove()}, 600);
	}).then(function() {
		$r.init();
	});
});