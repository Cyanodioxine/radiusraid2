var MinimapRender = function () { }

MinimapRender.prototype.render = function () {
	if(!$r.hardMode && $r.minimap.visible) {
		$r.ctxmg.fillStyle = $r.minimap.color;
		$r.ctxmg.fillRect($r.minimap.x, $r.minimap.y, $r.minimap.width, $r.minimap.height);

		$r.ctxmg.fillStyle = 'hsla(0, 0%, 100%, 0.1)';
		$r.ctxmg.fillRect(
			Math.floor($r.minimap.x + -$r.screen.x * $r.minimap.scale),
			Math.floor($r.minimap.y + -$r.screen.y * $r.minimap.scale),
			Math.floor($r.cw * $r.minimap.scale),
			Math.floor($r.ch * $r.minimap.scale)
		);

		//$r.ctxmg.beginPath();
		for (var i = 0; i < $r.enemies.length; i++) {
			var enemy = $r.enemies[i],
				size = enemy.radius,
				sizeOnMap = (size / 10 < 2) ? 2 : size / 10,
				x = $r.minimap.x + Math.floor(enemy.x * $r.minimap.scale),
				y = $r.minimap.y + Math.floor(enemy.y * $r.minimap.scale);
			if ($r.util.pointInRect(x + 1, y + 1, $r.minimap.x, $r.minimap.y, $r.minimap.width, $r.minimap.height)) {
				//$r.ctxmg.rect( x, y, 2, 2 );
				$r.ctxmg.fillStyle = 'hsl(' + enemy.hue + ', ' + enemy.saturation + '%, 50%)';
				$r.ctxmg.fillRect(x, y, sizeOnMap, sizeOnMap);
			}
		}
		//$r.ctxmg.fillStyle = '#f00';
		//$r.ctxmg.fill();

		$r.ctxmg.beginPath();
		for (var i = 0; i < $r.bullets.length; i++) {
			var bullet = $r.bullets[i],
				x = $r.minimap.x + Math.floor(bullet.x * $r.minimap.scale),
				y = $r.minimap.y + Math.floor(bullet.y * $r.minimap.scale);
			if ($r.util.pointInRect(x, y, $r.minimap.x, $r.minimap.y, $r.minimap.width, $r.minimap.height)) {
				$r.ctxmg.fillStyle = bullet.strokeStyle;
				$r.ctxmg.fillRect(x, y, 1, 1);
			}
		}
		$r.ctxmg.fill();

		$r.ctxmg.fillStyle = $r.hero.fillStyle;
		$r.ctxmg.fillRect($r.minimap.x + Math.floor($r.hero.x * $r.minimap.scale), $r.minimap.y + Math.floor($r.hero.y * $r.minimap.scale), 2, 2);

		$r.ctxmg.strokeStyle = $r.minimap.strokeColor;
		$r.ctxmg.strokeRect($r.minimap.x - 0.5, $r.minimap.y - 0.5, $r.minimap.width + 1, $r.minimap.height + 1);
	}
}