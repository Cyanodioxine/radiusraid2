var ScoreRender = function ()
{
	this.scoreLabel = null;
	this.scoreText = null;
	this.bestLabel = null;
	this.bestText = null;
}

ScoreRender.prototype.render = function () {
	if(!$r.hardMode) {
		$r.ctxmg.beginPath();
		this.scoreLabel = $r.text({
			ctx: $r.ctxmg,
			x: $r.ui.progress.progressBar.x + $r.ui.progress.progressBar.width + 40,
			y: 20,
			text: 'SCORE',
			hspacing: 1,
			vspacing: 1,
			halign: 'top',
			valign: 'left',
			scale: 2,
			snap: 1,
			render: 1
		});
		$r.ctxmg.fillStyle = 'hsla(0, 0%, 100%, 0.5)';
		$r.ctxmg.fill();

		$r.ctxmg.beginPath();
		this.scoreText = $r.text({
			ctx: $r.ctxmg,
			x: this.scoreLabel.ex + 10,
			y: 20,
			text: $r.util.pad($r.score, 6),
			hspacing: 1,
			vspacing: 1,
			halign: 'top',
			valign: 'left',
			scale: 2,
			snap: 1,
			render: 1
		});
		$r.ctxmg.fillStyle = 'hsla(0, 0%, 100%, 1)';
		$r.ctxmg.fill();

		$r.ctxmg.beginPath();
		this.bestLabel = $r.text({
			ctx: $r.ctxmg,
			x: $r.ui.progress.progressBar.x + $r.ui.progress.progressBar.width + 40,
			y: 40,
			text: 'BEST',
			hspacing: 1,
			vspacing: 1,
			halign: 'top',
			valign: 'left',
			scale: 2,
			snap: 1,
			render: 1
		});
		$r.ctxmg.fillStyle = 'hsla(0, 0%, 100%, 0.5)';
		$r.ctxmg.fill();

		$r.ctxmg.beginPath();
		this.bestText = $r.text({
			ctx: $r.ctxmg,
			x: this.bestLabel.ex + 10,
			y: 40,
			text: $r.util.pad($r.storage['score'], 6),
			hspacing: 1,
			vspacing: 1,
			halign: 'top',
			valign: 'left',
			scale: 2,
			snap: 1,
			render: 1
		});
		$r.ctxmg.fillStyle = 'hsla(0, 0%, 100%, 1)';
		$r.ctxmg.fill();
	}
}