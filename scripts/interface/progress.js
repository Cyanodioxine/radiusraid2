var ProgressRender = function ()
{
	this.progressText = null;
	this.progressBar = null;
}

ProgressRender.prototype.render = function () {
	$r.ctxmg.beginPath();
	this.progressText = $r.text({
		ctx: $r.ctxmg,
		x: $r.ui.health.healthBar.x + $r.ui.health.healthBar.width + 40,
		y: 20,
		text: 'PROGRESS',
		hspacing: 1,
		vspacing: 1,
		halign: 'top',
		valign: 'left',
		scale: 2,
		snap: 1,
		render: 1
	});
	$r.ctxmg.fillStyle = 'hsla(0, 0%, 100%, 0.5)';
	$r.ctxmg.fill();
	this.progressBar = {
		x: this.progressText.ex + 10,
		y: this.progressText.sy,
		width: $r.ui.health.healthBar.width,
		height: $r.ui.health.healthBar.height
	};
	$r.ctxmg.fillStyle = 'hsla(0, 0%, 20%, 1)';
	$r.ctxmg.fillRect(this.progressBar.x, this.progressBar.y, this.progressBar.width, this.progressBar.height);
	$r.ctxmg.fillStyle = 'hsla(0, 0%, 100%, 0.25)';
	$r.ctxmg.fillRect(this.progressBar.x, this.progressBar.y, this.progressBar.width, this.progressBar.height / 2);
	$r.ctxmg.fillStyle = 'hsla(0, ' + $r.hardMode * 100 + '%, 50%, 1)';
	$r.ctxmg.fillRect(this.progressBar.x, this.progressBar.y, ($r.level.kills / $r.level.killsToLevel) * this.progressBar.width, this.progressBar.height);
	$r.ctxmg.fillStyle = 'hsla(0, ' + $r.hardMode * 100 + '%, 100%, 1)';
	$r.ctxmg.fillRect(this.progressBar.x, this.progressBar.y, ($r.level.kills / $r.level.killsToLevel) * this.progressBar.width, this.progressBar.height / 2);

	if ($r.level.kills == $r.level.killsToLevel) {
		$r.particleEmitters.push(new $r.ParticleEmitter({
			x: -$r.screen.x + this.progressBar.x + this.progressBar.width,
			y: -$r.screen.y + this.progressBar.y + this.progressBar.height / 2,
			count: 30,
			spawnRange: 5,
			friction: 0.95,
			minSpeed: 2,
			maxSpeed: 25,
			minDirection: $r.pi / 2 - $r.pi / 4,
			maxDirection: $r.pi / 2 + $r.pi / 4,
			hue: 0,
			saturation: $r.hardMode * 100
		}));
	}
}