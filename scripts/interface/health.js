var HealthRender = function()
{
	this.healthText = null;
	this.healthBar = null;
}

HealthRender.prototype.render = function () {
	$r.ctxmg.beginPath();
	this.healthText = $r.text({
		ctx: $r.ctxmg,
		x: 20,
		y: 20,
		text: 'HEALTH',
		hspacing: 1,
		vspacing: 1,
		halign: 'top',
		valign: 'left',
		scale: 2,
		snap: 1,
		render: 1
	});
	$r.ctxmg.fillStyle = 'hsla(0, 0%, 100%, 0.5)';
	$r.ctxmg.fill();
	this.healthBar = {
		x: this.healthText.ex + 10,
		y: this.healthText.sy,
		width: 110,
		height: 10
	};
	$r.ctxmg.fillStyle = 'hsla(0, 0%, 20%, 1)';
	$r.ctxmg.fillRect(this.healthBar.x, this.healthBar.y, this.healthBar.width, this.healthBar.height);
	$r.ctxmg.fillStyle = 'hsla(0, 0%, 100%, 0.25)';
	$r.ctxmg.fillRect(this.healthBar.x, this.healthBar.y, this.healthBar.width, this.healthBar.height / 2);

	if(!$r.hardMode) {
		$r.ctxmg.fillStyle = 'hsla(' + $r.hero.life * 120 + ', 100%, 40%, 1)';
		$r.ctxmg.fillRect(this.healthBar.x, this.healthBar.y, ($r.hero.life / $r.hero.maxHealth) * this.healthBar.width, this.healthBar.height);
		$r.ctxmg.fillStyle = 'hsla(' + $r.hero.life * 120 + ', 100%, 75%, 1)';
		$r.ctxmg.fillRect(this.healthBar.x, this.healthBar.y, ($r.hero.life / $r.hero.maxHealth) * this.healthBar.width, this.healthBar.height / 2);
	} else {
		var w = this.healthBar.width / 2;

		$r.text({
			ctx: $r.ctxmg,
			x: this.healthBar.x + w,
			y: this.healthBar.y,
			text: '???',
			hspacing: 1,
			vspacing: 1,
			halign: 'center',
			valign: 'left',
			scale: 2,
			snap: 1,
			render: 1
		});
		$r.ctxmg.fillStyle = 'hsla(0, 0%, 100%, 1)';
		$r.ctxmg.fill();
	}

	if ($r.hero.takingDamage && $r.hero.life > 0.01) {
		$r.particleEmitters.push(new $r.ParticleEmitter({
			x: -$r.screen.x + this.healthBar.x + ($r.hero.life / $r.hero.maxHealth) * this.healthBar.width,
			y: -$r.screen.y + this.healthBar.y + this.healthBar.height / 2,
			count: 1,
			spawnRange: 2,
			friction: 0.85,
			minSpeed: 2,
			maxSpeed: 20,
			minDirection: $r.pi / 2 - 0.2,
			maxDirection: $r.pi / 2 + 0.2,
			hue: $r.hero.life * 120,
			saturation: 100
		}));
	}
}