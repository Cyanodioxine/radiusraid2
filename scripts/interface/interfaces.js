$r.ui = {}

function loadInterfaces()
{
	console.log("interface loaded!");
	
	setupInterfaces();
}

function setupInterfaces()
{
	$r.ui.health = new HealthRender();
	$r.ui.progress = new ProgressRender();
	$r.ui.score = new ScoreRender();
	//$r.ui.money = new $r.ui.MoneyRender();
	$r.ui.minimap = new MinimapRender();
}

function renderInterfaces()
{
	$r.ui.health.render();
	$r.ui.progress.render();
	$r.ui.score.render();
	//$r.ui.money.render();
	$r.ui.minimap.render();
}

$r.renderInterface = function() {
	/*==============================================================================
	 Powerup Timers
	 ==============================================================================*/
	for( var i = 0; i < $r.definitions.powerups.length; i++ ) {
		var powerup = $r.definitions.powerups[ i ],
			powerupOn = ( $r.powerupTimers[ i ] > 0 );
		$r.ctxmg.beginPath();

		var powerupText = $r.text( {
			ctx: $r.ctxmg,
			//x: $r.minimap.x + $r.minimap.width + 90,
			//y: $r.minimap.y + 4 + ( i * 12 ),
			x: 85,
			y: 40 + 4 + ( i * 12 ),
			text: powerup.title,
			hspacing: 1,
			vspacing: 1,
			halign: 'right',
			valign: 'top',
			scale: 1,
			snap: 1,
			render: 1
		} );
		if( powerupOn ) {
			$r.ctxmg.fillStyle = 'hsla(0, 0%, 100%, ' + ( 0.25 + ( ( $r.powerupTimers[ i ] / 300 ) * 0.75 ) ) + ')';
		} else {
			$r.ctxmg.fillStyle = 'hsla(0, 0%, 100%, 0.25)';
		}
		$r.ctxmg.fill();
		if( powerupOn ) {
			var powerupBar = {
				x: powerupText.ex + 5,
				y: powerupText.sy,
				width: 110,
				height: 5
			};
			$r.ctxmg.fillStyle = 'hsl(' + powerup.hue + ', ' + powerup.saturation + '%, ' + powerup.lightness + '%)';
			$r.ctxmg.fillRect( powerupBar.x, powerupBar.y, ( $r.powerupTimers[ i ] / 300 ) * powerupBar.width, powerupBar.height );
		}
	}

	/*==============================================================================
	 Instructions
	 ==============================================================================*/
	if( $r.instructionTick < $r.instructionTickMax ){
		$r.instructionTick += $r.dt;
		$r.ctxmg.beginPath();
		$r.text( {
			ctx: $r.ctxmg,
			x: $r.cw / 2 - 10,
			y: $r.ch - 20,
			text: 'MOVE\nAIM/FIRE\nAUTOFIRE\nPAUSE\nMUTE',
			hspacing: 1,
			vspacing: 17,
			halign: 'right',
			valign: 'bottom',
			scale: 2,
			snap: 1,
			render: 1
		} );
		if( $r.instructionTick < $r.instructionTickMax * 0.25 ) {
			var alpha = ( $r.instructionTick / ( $r.instructionTickMax * 0.25 ) ) * 0.5;
		} else if( $r.instructionTick > $r.instructionTickMax - $r.instructionTickMax * 0.25 ) {
			var alpha = ( ( $r.instructionTickMax - $r.instructionTick ) / ( $r.instructionTickMax * 0.25 ) ) * 0.5;
		} else {
			var alpha = 0.5;
		}
		alpha = Math.min( 1, Math.max( 0, alpha ) );

		$r.ctxmg.fillStyle = 'hsla(0, 0%, 100%, ' + alpha + ')';
		$r.ctxmg.fill();

		$r.ctxmg.beginPath();
		$r.text( {
			ctx: $r.ctxmg,
			x: $r.cw / 2 + 10,
			y: $r.ch - 20,
			text: 'WASD/ARROWS\nMOUSE\nF\nESC\nM',
			hspacing: 1,
			vspacing: 17,
			halign: 'left',
			valign: 'bottom',
			scale: 2,
			snap: 1,
			render: 1
		} );
		if( $r.instructionTick < $r.instructionTickMax * 0.25 ) {
			var alpha = ( $r.instructionTick / ( $r.instructionTickMax * 0.25 ) ) * 1;
		} else if( $r.instructionTick > $r.instructionTickMax - $r.instructionTickMax * 0.25 ) {
			var alpha = ( ( $r.instructionTickMax - $r.instructionTick ) / ( $r.instructionTickMax * 0.25 ) ) * 1;
		} else {
			var alpha = 1;
		}
		alpha = Math.min( 1, Math.max( 0, alpha ) );

		$r.ctxmg.fillStyle = 'hsla(0, 0%, 100%, ' + alpha + ')';
		$r.ctxmg.fill();
	}

	if( $r.slow || $r.emp ) {
		$r.ctxmg.fillStyle = 'hsla(200, 100%, 20%, 0.05)';
		$r.ctxmg.fillRect( 0, 0, $r.cw, $r.ch );
	}

	if($r.hardMode) {
		$r.ctxmg.beginPath();
		$r.text( {
			ctx: $r.ctxmg,
			x: 5,
			y: $r.ch - 5,
			text: 'KILLS: ' + $r.kills,
			hspacing: 1,
			vspacing: 17,
			halign: 'left',
			valign: 'bottom',
			scale: 2,
			snap: 1,
			render: 1
		} );
		$r.ctxmg.fillStyle = 'hsla(0, 0%, 100%, 1)';
		$r.ctxmg.fill();
	}
};